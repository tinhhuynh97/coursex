var oldCate = null;

function getCoursesByCategory(categoryName, pageNum, sort, minPrice, maxPrice) {
    if (currentMode !== Mode.CATEGORY ||
        oldCate == null ||
        (oldCate != null && oldCate !== categoryName)) {
        showRefreshSortFilter();
        document.getElementById("txtSearch").value = "";
    }
    oldCate = categoryName;

    displayLoader();
    var url = CONTEXT_PATH + "/getCoursesByCategory?"
        + "categoryName=" + categoryName +
        "&page=" + pageNum + "&sort=" + sort + "&minPrice=" + minPrice + "&maxPrice=" + maxPrice;
    loadXML(url, "GET", null, true, function (xhr) {
        var xmlDoc = xhr.responseXML;
        var xsltFilePath = CONTEXT_PATH + "/xsl/course_list_paging_response.xsl";
        console.log(xmlDoc.toString());
        transformCourseListXMLWithXSTL(xmlDoc, xsltFilePath);
        var title = document.getElementById("all-course-title");
        title.innerText = categoryName;
        currentMode = Mode.CATEGORY;
        currentCategory = categoryName;
        addOnClickForPage(Mode.CATEGORY, sort, minPrice, maxPrice);
        addFavoriteBtnForCourses("course-list-container");
    });
}
