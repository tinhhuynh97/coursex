function route() {
    console.log(currentMode);
    switch (currentMode) {
        case Mode.ALL:
            getAllCoursesByPage(currentPage, currentSort, currentMinPrice, currentMaxPrice);
            break;
        case Mode.CATEGORY:
            getCoursesByCategory(currentCategory, currentPage, currentSort, currentMinPrice, currentMaxPrice);
            break;
        case Mode.SEARCH:
            searchCourses(currentSearch, currentPage, currentSort, currentMinPrice, currentMaxPrice);
            break;
        case Mode.FAVORITE:
            getFavoriteCourses(currentPage, currentSort, currentMinPrice, currentMaxPrice);
            break;
    }
    if (sessionStorage.getItem("wait_auth_course_id")) {
        addFavoriteCourse(sessionStorage.getItem("wait_auth_course_id"));
        sessionStorage.removeItem("wait_auth_course_id");
    }
    sessionStorage.removeItem("wait_auth_continue_task");
}