window.onload = function () {
    getAllCategories();
    getAllCoursesByPage(1, null, null, null);
    toggleLoginRegisterUserDiv();
    if (window.sessionStorage.getItem("wait_auth_continue_task") &&
        window.sessionStorage.getItem("wait_auth_continue_task") === 'true') {
        route();
    }
    if (getCookie(CkKey.AuthEmail) && getCookie(CkKey.AuthRole) && getCookie(CkKey.AuthRole) === Role.NORMAL) {
        getRecommendationCourses();
    }
};

function toggleLoginRegisterUserDiv() {
    if (getCookie(CkKey.AuthRole) !== null
        && getCookie(CkKey.AuthEmail) !== null) {
        document.getElementById('login-register-container').style.display = 'none';
        document.getElementById('user-container').style.display = 'inline-block';
        document.getElementById('username-span').innerText = getCookie(CkKey.AuthUsername);
        setUpUserFunctionsList();
    } else {
        document.getElementById('login-register-container').style.display = 'inline-block';
        document.getElementById('user-container').style.display = 'none';
        document.getElementById('username-span').innerText = '';
    }
}

function setUpUserFunctionsList() {
    var content = document.getElementById('user-function-content');
    if (getCookie(CkKey.AuthRole) === Role.ADMIN) {
        var admin = "<a href='/coursex/admin.jsp'>Trang admin</a>";
        content.innerHTML += admin;
    } else if (getCookie(CkKey.AuthRole) === Role.NORMAL) {
        var favoriteCourses = "<a onclick='getFavoriteCourses(1, null, null, null);'>Khoá học yêu thích</a>";
        content.innerHTML += favoriteCourses;
    }
}

function showUserFunctions() {
    var content = document.getElementById("user-function-content");
    console.log(content.className);
    if (content.className === 'user-function-content') {
        content.className = "user-function-content-show";
    } else if (content.className === 'user-function-content-show') {
        content.className = "user-function-content";
    }
}


function transformCourseListXMLWithXSTL(xmlDoc, xsltFilePath) {
    var params = {};
    params['container-id'] = 'course-list-container';
    applyXSLTToXMLDoc(xmlDoc, xsltFilePath, params, function (html) {
        hideLoader();
        removeCourseList();
        removePagination();
        var section = document.getElementById("course-list-section");
        section.appendChild(html);
        showNoCourseSpanIfListEmpty();
    });
}


function transformCategoryXMLWithXSTL(xmlDoc, xsltFilePath) {
    applyXSLTToXMLDoc(xmlDoc, xsltFilePath, null, function (html) {
        hideLoader();
        var cateContent = document.getElementById("category-dropdown-content");
        cateContent.appendChild(html);
    });
}

function removeCourseList() {
    removeChild("course-list-section", "course-list-container");
}

function removePagination() {
    removeChild("course-list-section", "pagination");
}

function removeNumOfCourses() {
    removeChild("course-list-section", "number-of-result");
}

function removeChild(parentId, childId) {
    var parent = document.getElementById(parentId);
    var child = document.getElementById(childId);
    if (child && parent.contains(child)) {
        console.log(childId);
        parent.removeChild(child);
    }
}


function displayLoader() {
    hideNoCourseSpan();
    removeCourseList();
    removePagination();
    removeNumOfCourses();
    var loader = document.getElementById("loader");
    if (loader != null) {
        loader.style.display = "block";
    }
}

function hideLoader() {
    removeCourseList();
    removePagination();
    removeNumOfCourses();
    var loader = document.getElementById("loader");
    if (loader != null) {
        loader.style.display = "none";
    }
}

function showNoCourseSpanIfListEmpty() {
    var courseListContainer = document.getElementById("course-list-container");
    if (courseListContainer != null && courseListContainer.childElementCount === 0) {
        var noCourseSpan = document.getElementById("no-course");
        if (noCourseSpan != null && (noCourseSpan.style.display === "" || noCourseSpan.style.display === "none")) {
            console.log("noCourseSpan none");
            noCourseSpan.style.display = "block";
        }
    }
}

function hideNoCourseSpan() {
    var noCourseSpan = document.getElementById("no-course");
    if (noCourseSpan != null && noCourseSpan.style.display === "block") {
        noCourseSpan.style.display = "none";
    }
}

function refreshInputs() {
    refreshSortFilter();
    document.getElementById("txtSearch").value = "";
}

function refreshSortFilter() {
    var menu = document.getElementById("sort-menu");
    menu.value = SortCriteria.NONE;
    document.getElementById("txtMinPrice").value = "";
    document.getElementById("txtMaxPrice").value = "";
}

function showRefreshSortFilter() {
    document.getElementById("sort-filter-options").style.display = 'inline-block';
    refreshSortFilter();
}

function hideRefreshSortFilter() {
    document.getElementById("sort-filter-options").style.display = 'none';
    refreshSortFilter();

}

function addOnClickForPage(mode, sort, minPrice, maxPrice) {
    var pagination = document.getElementById("pagination");
    var pages = pagination.children;

    for (var i = 0; i < pages.length; i++) {
        (function () {
            var page = pages[i];
            var pageNum = page.getAttribute('data-pagenum');
            switch (mode) {
                case Mode.ALL:
                    page.onclick = function (event) {
                        getAllCoursesByPage(pageNum, sort, minPrice, maxPrice);
                    };
                    break;
                case Mode.CATEGORY:
                    page.onclick = function (event) {
                        getCoursesByCategory(currentCategory, pageNum, sort, minPrice, maxPrice)
                    };
                    break;
                case Mode.SEARCH:
                    page.onclick = function (event) {
                        searchCourses(currentSearch, pageNum, sort, minPrice, maxPrice)
                    };
                    break;
                case Mode.FAVORITE:
                    page.onclick = function (event) {
                        getFavoriteCourses(pageNum, sort, minPrice, maxPrice)
                    };
                    break;
            }
        })();

    }
}

