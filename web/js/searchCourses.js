var oldSearch = null;

function searchCourses(search, page, sort, minPrice, maxPrice) {
    if (currentMode !== Mode.SEARCH ||
        oldSearch == null ||
        (oldSearch != null && oldSearch !== search)) {
        showRefreshSortFilter();
    }
    oldSearch = search;
    displayLoader();
    var url = CONTEXT_PATH + "/searchCourses?search=" + search + "&page=" + page + "&sort="
        + sort + "&minPrice=" + minPrice + "&maxPrice=" + maxPrice;
    loadXML(url, "GET", null, true, function (xhr) {
        var xmlDoc = xhr.responseXML;
        var xsltFilePath = CONTEXT_PATH + "/xsl/course_list_paging_response.xsl";
        console.log(xmlDoc.toString());
        transformCourseListXMLWithXSTL(xmlDoc, xsltFilePath);
        var title = document.getElementById("all-course-title");
        title.innerText = "Tìm kiếm: " + search;

        currentSearch = search;
        currentMode = Mode.SEARCH;
        addOnClickForPage(currentMode, sort, minPrice, maxPrice);
        addFavoriteBtnForCourses("course-list-container");

    });

}

function onEnterSearch(event, search, page) {
    if (event.which === 13 || event.keyCode === 13) {
        searchCourses(search, page);
        return false;
    }
}
