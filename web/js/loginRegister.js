function showSnackBarThenGoToUrl(message, url) {
    var snackbar = document.getElementById("snackbar");
    snackbar.className = "show";
    snackbar.innerText = message;
    setTimeout(function () {
        snackbar.className = snackbar.className.replace("show", "");
        if (url != null) {
            window.location.href = url;
        }
    }, 1200);
}


function login() {
    var email = document.getElementById("txtEmail").value;
    var password = document.getElementById("txtPassword").value;
    var url = CONTEXT_PATH + "/login";
    var data = "email=" + email + "&password=" + password;
    loadXML(url, "POST", data, true, function (xhr) {
        var loginResponse = xhr.responseXML;
        var code = getSingleNodeValueXPath("//*[local-name()='Code']/text()", loginResponse).nodeValue;
        if (code === AuthCode.LOGIN_SUCCESS) {
            var user = {
                email: getSingleNodeValueXPath("//*[local-name()='email']/text()", loginResponse).nodeValue,
                username: getSingleNodeValueXPath("//*[local-name()='username']/text()", loginResponse).nodeValue,
                role: getSingleNodeValueXPath("//*[local-name()='role']/text()", loginResponse).nodeValue
            };
            console.log(user.email + " " + user.username + " " + user.role.nodeValue + " " + code);
            setCookie(CkKey.AuthUsername, user.username, 1);
            setCookie(CkKey.AuthEmail, user.email, 1);
            setCookie(CkKey.AuthRole, user.role, 1);
            showSnackBarThenGoToUrl("Đăng nhập thành công", "/coursex/index.jsp");
        } else if (code === AuthCode.LOGIN_FAILED) {
            showSnackBarThenGoToUrl("Đăng nhập thất bại. Vui lòng thử lại", null);
        }

    });
}

function logout() {
    eraseCookie(CkKey.AuthEmail);
    eraseCookie(CkKey.AuthRole);
    eraseCookie(CkKey.AuthUsername);
    window.location.reload();
}