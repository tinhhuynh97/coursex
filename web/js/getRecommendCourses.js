function getRecommendationCourses() {
    var url = CONTEXT_PATH + "/getRecommendCourses";
    var data = "email=" + getCookie(CkKey.AuthEmail);
    loadXML(url, 'POST', data, true, function (xhr) {
        var response = xhr.responseXML;
        var xsdPath = CONTEXT_PATH + "/xsl/course_list.xsl";
        var params = {};
        params['container-id'] = 'recommend-course-container';
        applyXSLTToXMLDoc(response, xsdPath, params, function (courseHtml) {
            document.getElementById("recommend-course-list-section").style.display = 'inline-block';
            var section = document.getElementById('recommend-course-list-section');
            var container = courseHtml.getElementById('recommend-course-container');
            if (container.childElementCount === 0) {
                section.style.display = 'none';

            } else {
                section.appendChild(courseHtml);
                addFavoriteBtnForCourses("recommend-course-container");
            }
        });
    })
}

