window.onload = function (ev) {
    if (getCookie(CkKey.AuthRole) !== null
        && getCookie(CkKey.AuthEmail) !== null) {
        if (getCookie(CkKey.AuthRole) === Role.ADMIN) {
            checkCrawlerStatus();
        } else {
            document.getElementById("admin_content").style.display = 'none';
        }
    } else {
        document.getElementById("admin_content").style.display = 'none';
    }
    toggleLoginRegisterUserDiv();
};

function toggleLoginRegisterUserDiv() {
    if (getCookie(CkKey.AuthRole) !== null
        && getCookie(CkKey.AuthEmail) !== null) {
        document.getElementById('login-register-container').style.display = 'none';
        document.getElementById('user-container').style.display = 'inline-block';
        document.getElementById('username-span').innerText = getCookie(CkKey.AuthUsername);
    } else {
        document.getElementById('login-register-container').style.display = 'inline-block';
        document.getElementById('user-container').style.display = 'none';
        document.getElementById('username-span').innerText = '';
    }
}

function startCrawler() {
    var url = CONTEXT_PATH + '/startCrawler';
    var email = "email=" + getCookie(CkKey.AuthEmail);
    loadXML(url, 'POST', email, true, function (xhr) {
        handleResponse(xhr.responseXML);
    });
}


function stopCrawler() {
    var url = CONTEXT_PATH + '/stopCrawler';
    var email = "email=" + getCookie(CkKey.AuthEmail);
    loadXML(url, 'POST', email, true, function (xhr) {
        handleResponse(xhr.responseXML);
    });
}

function checkCrawlerStatus() {
    var url = CONTEXT_PATH + '/getCrawlerStatus';
    var email = "email=" + getCookie(CkKey.AuthEmail);
    loadXML(url, 'POST', email, true, function (xhr) {
        handleResponse(xhr.responseXML);
    });
}

function handleResponse(response) {
    var status = getSingleNodeValueXPath("//*[local-name()='Status']/text()", response).nodeValue;
    var delayForNextCrawl = null;
    if (status === CrawlerStatus.WORKING) {
        delayForNextCrawl = getSingleNodeValueXPath("//*[local-name()='Delay']/text()", response).nodeValue;
    }
    setUpCrawlerControlButtons(status, delayForNextCrawl);
}

var interval;

function setUpCrawlerControlButtons(status, delayForNextCrawl) {
    var startBtn = document.getElementById('btn-start');
    var stopBtn = document.getElementById('btn-stop');
    var statusSpan = document.getElementById('crawler-status');
    switch (status) {
        case CrawlerStatus.IDLING:
            startBtn.className = 'control-btn';
            stopBtn.className = 'control-btn-disable';
            statusSpan.className = 'crawler-status-yellow';
            break;
        case CrawlerStatus.TERMINATED:
            startBtn.className = 'control-btn';
            stopBtn.className = 'control-btn-disable';
            statusSpan.className = 'crawler-status-red';
            break;
        case CrawlerStatus.WORKING:
            startBtn.className = 'control-btn-disable';
            stopBtn.className = 'control-btn';
            statusSpan.className = 'crawler-status-green';
            break;

    }
    statusSpan.innerText = formatCrawlerStatusToVN(status);
    if (status === CrawlerStatus.WORKING && delayForNextCrawl !== null) {
        console.log(status + "-" + delayForNextCrawl);
        updateDelayInfo(delayForNextCrawl);
    } else {
        if (interval !== null) {
            clearInterval(interval);
        }
        document.getElementById('delay').innerText = '';
    }
}

function formatCrawlerStatusToVN(status) {
    switch (status) {
        case CrawlerStatus.WORKING:
            status = CrawlerStatusVN.WORKING;
            break;
        case CrawlerStatus.IDLING:
            status = CrawlerStatusVN.IDLING;
            break;
        case CrawlerStatus.TERMINATED:
            status = CrawlerStatusVN.TERMINATED;
            break;
    }
    return status;
}

function updateDelayInfo(delayForNextCrawl) {
    console.log(delayForNextCrawl);
    var delaySpan = document.getElementById('delay');
    if (delayForNextCrawl !== null) {
        delaySpan.innerText = ", sẵn sàng cho lần cào kế tiếp sau: "
            + convertTimeStrFromSeconds(delayForNextCrawl);

        interval = setInterval(function () {
            if (delayForNextCrawl > 0) {
                delayForNextCrawl -= 1;
                delaySpan.innerText = ", sẵn sàng cho lần cào kế tiếp sau: "
                    + convertTimeStrFromSeconds(delayForNextCrawl);
            } else {
                checkCrawlerStatus();
                clearInterval(interval);
            }
        }, 1000);
    } else {
        clearInterval(interval);
    }
}

function convertTimeStrFromSeconds(seconds) {
    var hours = Math.floor(seconds / (60 * 60));
    var remainder = Math.floor(seconds % 3600);
    var minutes = Math.floor(remainder / 60);
    seconds = Math.round(remainder % 60);
    return hours + " giờ " + minutes + " phút " + seconds + " giây.";
}