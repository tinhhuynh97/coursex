
function getAllCategories() {
    var url = CONTEXT_PATH + "/getAllCategories";
    loadXML(url, "GET", null, true, function (xhr) {
        var xmlDoc = xhr.responseXML;
        var xsltFilePath = "xsl/categories.xsl";
        transformCategoryXMLWithXSTL(xmlDoc, xsltFilePath);
    });

}


