var CONTEXT_PATH = "/coursex";

var Mode = {
    ALL: 'all',
    CATEGORY: 'category',
    SEARCH: 'search',
    FAVORITE: 'favorite'
};

var currentMode = Mode.ALL;
var currentSearch;
var currentCategory;
var currentPage;
var currentSort;
var currentMinPrice;
var currentMaxPrice;


var AuthCode = {
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILED: 'LOGIN_FAILED'
};

var Role = {
    ADMIN: 'ADMIN',
    NORMAL: 'NORMAL'
};

var SortCriteria = {
    NONE: 'none',
    PRICEASC: 'priceAsc',
    PRICEDESC: 'priceDesc'
};

var CkKey = {
    AuthUsername: 'auth_username',
    AuthEmail: 'auth_email',
    AuthRole: 'auth_role'
};

var CrawlerStatus = {
    IDLING: 'IDLING',
    STARTED: 'STARTED',
    WORKING: 'WORKING',
    PAUSED: 'PAUSED',
    SLEEPING: 'SLEEPING',
    TERMINATED: 'TERMINATED'
};

var CrawlerStatusVN = {
    IDLING: 'Đang rảnh',
    STARTED: 'Đã bắt đầu',
    WORKING: 'Đang làm việc',
    PAUSED: 'Tạm dừng',
    SLEEPING: 'Đang nghỉ',
    TERMINATED: 'Đã dừng'
};

var ReponseCode = {
    SUCCESS: 'SUCCESS',
    FAILED: 'FAILED'
};

