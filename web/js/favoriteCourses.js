function addFavoriteCourse(courseId) {
    if (!getCookie(CkKey.AuthUsername)) {
        window.sessionStorage.setItem("wait_auth_continue_task", "true");
        window.sessionStorage.setItem("wait_auth_course_id", courseId);
        window.location.href = CONTEXT_PATH + "/login_register.jsp";
    } else {
        var url = CONTEXT_PATH + "/addFavoriteCourse";
        var data = "email=" + getCookie(CkKey.AuthEmail) + "&courseId=" + courseId;
        loadXML(url, "POST", data, true, function (xhr) {
            var response = xhr.responseXML;
            var code = getSingleNodeValueXPath("//*[local-name()='Code']/text()", response).nodeValue;
            if (code === ReponseCode.SUCCESS) {
                alert("Đã thêm vào mục yêu thích!!!");
            } else if (code === ReponseCode.FAILED) {
                var message = getSingleNodeValueXPath("//*[local-name()='Message']/text()", response).nodeValue;
                alert(message);
            }
        })
    }
}


function removeFavoriteCourse(courseId) {
    var url = CONTEXT_PATH + "/removeFavoriteCourses";
    var data = "email=" + getCookie(CkKey.AuthEmail) + "&courseId=" + courseId;
    loadXML(url, "POST", data, true, function (xhr) {
        var response = xhr.responseXML;
        var code = getSingleNodeValueXPath("//*[local-name()='Code']/text()", response).nodeValue;
        if (code === ReponseCode.SUCCESS) {
            removeCourseArticle("course-list-container", courseId);
            alert("Xóa khỏi vào mục yêu thích!!!");
        } else if (code === ReponseCode.FAILED) {
            var message = getSingleNodeValueXPath("//*[local-name()='Message']/text()", response).nodeValue;
            alert(message);
        }
    })
}

function removeCourseArticle(parentId, courseId) {
    courseId = Number(courseId);
    var section = document.getElementById(parentId);
    var articles = section.children;
    for (var i = 0; i < articles.length; i++) {
        var article = articles[i];
        var id = Number(article.getAttribute("data-id"));
        if (courseId === id) {
            console.log("die plzz");
            article.parentNode.removeChild(article);
        }
    }
}


function getFavoriteCourses(pageNum, sort, minPrice, maxPrice) {
    if (currentMode !== Mode.FAVORITE) {
        refreshSortFilter();
    }
    displayLoader();
    var url = CONTEXT_PATH + "/getFavoriteCourses";
    var data = "email=" + getCookie(CkKey.AuthEmail) + "&page=" + pageNum + "&sort=" + sort + "&minPrice="
        + minPrice + "&maxPrice=" + maxPrice;
    loadXML(url, "POST", data, true, function (xhr) {
        var xmlDoc = xhr.responseXML;
        var xsltFilePath = CONTEXT_PATH + "/xsl/course_list_paging_response.xsl";
        transformCourseListXMLWithXSTL(xmlDoc, xsltFilePath);
        var title = document.getElementById("all-course-title");
        title.innerText = "Khoá học yêu thích";

        currentMode = Mode.FAVORITE;
        addOnClickForPage(currentMode, null, null, null);
        addRemoveBtnToCourses("course-list-container")
    })
}

function addFavoriteBtnForCourses(parentId) {
    if (getCookie(CkKey.AuthRole) === Role.NORMAL) {
        var section = document.getElementById(parentId);
        var articles = section.children;
        for (var i = 0; i < articles.length; i++) {
            var article = articles[i];
            var courseId = article.getAttribute("data-id");
            var btnHtml = "<span class='save-to-favorite-btn' title='Thêm vào mục yêu thích' onclick='addFavoriteCourse(" + courseId + ");'>❤</span>";
            article.innerHTML += btnHtml;
        }
    }
}

function addRemoveBtnToCourses(parentId) {
    if (getCookie(CkKey.AuthRole) === Role.NORMAL) {
        var section = document.getElementById(parentId);
        var articles = section.children;
        for (var i = 0; i < articles.length; i++) {
            console.log("hee" + articles.length);
            var article = articles[i];
            var courseId = article.getAttribute("data-id");
            var btnHtml = "<span class='save-to-favorite-btn' title='Xóa khỏi mục yêu thích' onclick='removeFavoriteCourse(" + courseId + ");'>❌</span>";
            article.innerHTML += btnHtml;
        }
    }
}