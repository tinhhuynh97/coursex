function getAllCoursesByPage(pageNum, sort, minPrice, maxPrice) {

    var url = CONTEXT_PATH + "/getAllCourses?page=" + pageNum + "&sort=" +
        sort + "&minPrice=" + minPrice + "&maxPrice=" + maxPrice;
    if (currentMode !== Mode.ALL) {
        showRefreshSortFilter();
        document.getElementById("txtSearch").value = "";
    }
    displayLoader();
    loadXML(url, "GET", null, true, function (xhr) {
        var xmlDoc = xhr.responseXML;
        var xsltFilePath = CONTEXT_PATH + "/xsl/course_list_paging_response.xsl";
        transformCourseListXMLWithXSTL(xmlDoc, xsltFilePath);
        var title = document.getElementById("all-course-title");
        title.innerText = "Tất cả khóa học";

        currentMode = Mode.ALL;
        currentPage = pageNum;
        addOnClickForPage(currentMode, sort, minPrice, maxPrice);
        addFavoriteBtnForCourses("course-list-container");
    });
}




