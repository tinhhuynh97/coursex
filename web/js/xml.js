// xml
function createXhr() {
    var xhr = null;
    try {
        xhr = new XMLHttpRequest();
    } catch (e) {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP")
        } catch (e) {
            xhr = new ActiveXObject("Microsoft.XMLHTP")
        }
    }
    return xhr;
}

function loadXML(url, method, data, async, successCallback) {
    var xhr = createXhr();
    if (xhr == null) {
        alert("Chúng tôi không thể thực hiện yêu cầu với trình duyệt này!" +
            " Vui lòng sử dụng trình duyệt khác: Chrome, Firefox,...");
        return;
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            successCallback(xhr);
        }
    };
    xhr.open(method, url, async);
    if (data != null) {
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    xhr.send(data);
}


function applyXSLTToXMLDoc(xmlDoc, xsltFilePath, params, successCallback) {
    loadXML(xsltFilePath, "GET", null, false, function (xhr) {
        var html;
        var xsltDoc = xhr.responseXML;
        if (window.ActiveXObject || xhr.responseType === "msxml-document") {
            var template = new ActiveXObject('Msxml2.XslTemplate');
            template.stylesheet = xsltDoc;
            var proc = template.createProcessor();
            proc.input = xmlDoc;
            if (params) {
                Object.keys(params).forEach(function (key) {
                    var value = params[key];
                    proc.addParameter(key, value);
                });
            }

            proc.transform();
            html = proc.output;
        }
        else if (typeof XSLTProcessor !== 'undefined') {
            var xsltProcessor = new XSLTProcessor();
            xsltProcessor.importStylesheet(xsltDoc);
            if (params) {
                Object.keys(params).forEach(function (key) {
                    var value = params[key];
                    xsltProcessor.setParameter(null, key, value);
                });
            }
            html = xsltProcessor.transformToFragment(xmlDoc, document);
        }
        successCallback(html);
    });
}


function getSingleNodeValueXPath(xpath, node) {
    return node.evaluate(xpath, node, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

