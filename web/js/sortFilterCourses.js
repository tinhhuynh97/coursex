


function sortFilterCourses(pageNum) {
    var menu = document.getElementById("sort-menu");
    var selectedOption = menu.options[menu.selectedIndex];
    var sortCriteria = selectedOption.value;
    var txtMinPrice = document.getElementById("txtMinPrice").value;
    var txtMaxPrice = document.getElementById("txtMaxPrice").value;
    var minPrice = parseIntFromStr(txtMinPrice);
    var maxPrice = parseIntFromStr(txtMaxPrice);
    switch (currentMode) {
        case Mode.ALL:
            getAllCoursesByPage(pageNum, sortCriteria, minPrice, maxPrice);
            break;
        case Mode.CATEGORY:
            getCoursesByCategory(currentCategory, pageNum, sortCriteria, minPrice, maxPrice);
            break;
        case Mode.SEARCH:
            searchCourses(currentSearch, pageNum, sortCriteria, minPrice, maxPrice);
            break;

        case Mode.FAVORITE:
            getFavoriteCourses(pageNum, sortCriteria, minPrice, maxPrice);
            break;

    }
}

function parseIntFromStr(string) {
    if (!isNaN(string)) {
        return string;
    } else {
        return null;
    }
}

