<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    <xsl:param name="container-id"/>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*[local-name()='Courses']">
        <xsl:if test="boolean(./@*[local-name()='numOfCourses'])">
            <span id="number-of-result" class="number-of-result">Có
                <b>
                    <xsl:value-of select="./@*[local-name()='numOfCourses']"/>
                </b>
                kết quả
            </span>
        </xsl:if>
        <section id="{$container-id}" class="course-list-container">
            <xsl:apply-templates/>
        </section>
    </xsl:template>

    <xsl:template match="*[local-name()='Course']">
        <article class="course-container" data-id="{./@*[local-name()='id']}">
            <a href="{./*[local-name()='Url']}" class="course-container-link" target="_blank">
                <xsl:apply-templates/>
            </a>
        </article>
    </xsl:template>

    <xsl:template match="*[local-name()='img']">
        <img src="{.}" class="course-img" onerror="if (this.src != 'img/no_img.jpg') this.src = 'img/no_img.jpg';"/>
    </xsl:template>

    <xsl:template match="*[local-name()='Name']">
        <a class="course-title-link" href="{./../*[local-name()='Url']}"
           title="{.}" target="_blank">
            <b class="course-title">
                <xsl:value-of select="."/>
            </b>
        </a>
    </xsl:template>

    <xsl:template match="*[local-name()='LecturerName']">
        <span class="course-lecturer" title="{.}">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <xsl:decimal-format name="df"/>
    <xsl:template match="*[local-name()='SellPrice']">
        <span class="course-price">
            <b>
                <xsl:value-of select="format-number(number(.), '#,###', 'df')"/>đ
            </b>
        </span>
    </xsl:template>

    <xsl:template match="*[local-name()='Supplier']">
        <a href="{./*[local-name()='SupplierUrl']}" title="{./*[local-name()='SupplierName']}" target="_blank"
           class="origin-site-link">
            <img src="{./*[local-name()='LogoUrl']}" class="origin-site-logo"/>
        </a>
    </xsl:template>

    <xsl:template match="*[local-name()='Url']">
        <span></span>
    </xsl:template>

</xsl:stylesheet>

