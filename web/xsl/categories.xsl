<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsi:template match="*[local-name()='Categories']">
        <span onclick="getAllCoursesByPage(1, 'none', null, null);">Tất cả</span>
        <xsl:apply-templates/>
    </xsi:template>

    <xsi:template match="*[local-name()='Category']">
        <span data-id="{.//@*[local-name()='id']}" onclick="getCoursesByCategory('{.//*[local-name()='CategoryName']}', 1);">
            <xsl:value-of select=".//*[local-name()='CategoryName']"/>
        </span>
    </xsi:template>
</xsl:stylesheet>