<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*[local-name()='Pages']">
        <div id="pagination" class="pagination">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="*[local-name()='Page']">
        <xsl:choose>
            <xsl:when test="./@*[local-name()='active' and .='true']">
                <div data-pagenum="{./@*[local-name()='pageNum']}" class="active"
                     title="trang {./@*[local-name()='pageNum']}">
                    <xsl:apply-templates select="./@*[local-name()='pageNum']"/>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div data-pagenum="{./@*[local-name()='pageNum']}" title="trang {./@*[local-name()='pageNum']}">
                    <xsl:apply-templates select="./@*[local-name()='pageNum']"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="@*[local-name()='pageNum']">
        <xsl:choose>
            <xsl:when test="./../@*[local-name()='previous' and .='true']">
                <xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>
            </xsl:when>
            <xsl:when test="./../@*[local-name()='next' and .='true']">
                <xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>

