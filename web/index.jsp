<%--
  Created by IntelliJ IDEA.
  User: charl
  Date: 12/06/2018
  Time: 8:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/xml.js"></script>
    <script type="text/javascript" src="js/getAllCategories.js"></script>
    <script type="text/javascript" src="js/getAllCourses.js"></script>
    <script type="text/javascript" src="js/getCoursesByCategory.js"></script>
    <script type="text/javascript" src="js/searchCourses.js"></script>
    <script type="text/javascript" src="js/sortFilterCourses.js"></script>
    <script type="text/javascript" src="js/favoriteCourses.js"></script>
    <script type="text/javascript" src="js/config.js"></script>
    <script type="text/javascript" src="js/getRecommendCourses.js"></script>
    <script type="text/javascript" src="js/loginRegister.js"></script>
    <script type="text/javascript" src="js/cookie.js"></script>
    <script type="text/javascript" src="js/route.js"></script>
    <title>CourseX - Nơi tìm kiếm các khóa học online</title>
</head>
<body>
<header>
    <div class="favicon-container">
        <a href="${pageContext.request.contextPath}/">
            <img src="img/favicon.png"/>
        </a>
    </div>

    <div class="category-dropdown-menu">
        <button class="category-dropdown-btn">Danh mục</button>
        <div id="category-dropdown-content" class="category-dropdown-content">

        </div>
    </div>
    <div class="search-container">
        <input type="text" placeholder="Tìm kiếm khóa học..." id="txtSearch"
               onkeypress="onEnterSearch(event, document.getElementById('txtSearch').value, 1);">
        <button onclick="searchCourses(document.getElementById('txtSearch').value, 1, null, null, null);">Search
        </button>
    </div>

    <div class="login-register-user">
        <div id="login-register-container" class="login-register-container">
            <a class="login-register-link" href="login_register.jsp">Đăng kí/Đăng nhập</a>
        </div>
        <div id="user-container" class="user-container" style="display: none">
            <div id="user-function-dropdown" class="user-function-dropdown" onclick="showUserFunctions();">
                Xin chào, <span id="username-span"></span>
                <div id="user-function-content" class="user-function-content">
                </div>
            </div>
            <a onclick="logout();">Đăng xuất</a>
        </div>
    </div>


</header>
<div id="main-content" class="main-content">

    <div class="all-course-title-container">
        <span id="all-course-title" class="all-course-title">Tất cả khóa học</span>
    </div>

    <div id="sort-filter-options" class="sort-filter-options">
        <div id="sort-wrapper" class="sort-wrapper">
            Sắp xếp theo:
            <select id="sort-menu" class="sort-menu" onchange="sortFilterCourses(1);">
                <option value="none" selected="selected">Không</option>
                <option value="priceAsc">Giá tăng dần</option>
                <option value="priceDesc">Giá giảm dần</option>
            </select>
        </div>

        <div id="filter-wrapper" class="filter-wrapper">
            <input id="txtMinPrice" value="" placeholder="giá thấp nhất..."/>
            <input id="txtMaxPrice" value="" placeholder="giá cao nhất..."/>
            <button onclick="sortFilterCourses(1);">Lọc</button>
        </div>

    </div>

    <span class="no-course" id="no-course">Rất tiếc, không thể tìm thấy khóa học theo yêu cầu.<br/>
    Chúng tôi đang cập nhật thêm khóa học, mong bạn thông cảm.</span>

    <section id="course-list-section" class="course-list-section">
        <div id="loader" class="loader"></div>
    </section>

    <section id="recommend-course-list-section">
        <span id="title-recommend-courses">Đề xuất cho bạn</span>
    </section>


</div>
<footer>
    CourseX - Nơi tìm kiếm các khóa học online<br>
    <%--Made with 💖 by <a href="https://github.com/TinhHuynh" target="_blank">Tinh Huynh</a>--%>
</footer>
</body>
</html>
