<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Đăng ký / Đăng Nhập</title>
    <script type="text/javascript" src="js/xml.js"></script>
    <script type="text/javascript" src="js/loginRegister.js"></script>
    <script type="text/javascript" src="js/config.js"></script>
    <script type="text/javascript" src="js/cookie.js"></script>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/login_register.css">
</head>
<body>
<header>
    <div class="favicon-container">
        <a href="${pageContext.request.contextPath}/">
            <img src="img/favicon.png"/>
        </a>
    </div>
</header>

<section class="main-content">
    <div class="login-register-container">
        <div class="login-container">
            <span class="label"> Đăng nhập</span>
            <input type="text" id="txtEmail" class="login-text-input" placeholder="Email"/> <br/>
            <input type="password" id="txtPassword" placeholder="Password"/> <br/>
            <input id="btnLogin" type="button" onclick="login()" value="Đăng nhập"/>
        </div>
        <div class="vertical-divider"></div>

        <div class="register-container">

        </div>
    </div>

</section>
<footer>
    CourseX - Nơi tìm kiếm các khóa học online<br>
    <%--Made with 💖 by <a href="https://github.com/TinhHuynh" target="_blank">Tinh Huynh</a>--%>
</footer>
<div id="snackbar">Đăng nhập thành công !!!</div>
</body>
</html>
