<%--
  Created by IntelliJ IDEA.
  User: charl
  Date: 10/07/2018
  Time: 3:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <script src="js/admin.js" type="text/javascript"></script>
    <script src="js/xml.js" type="text/javascript"></script>
    <script src="js/config.js" type="text/javascript"></script>
    <script src="js/cookie.js" type="text/javascript"></script>
    <script src="js/loginRegister.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/common.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
</head>
<body>
<header>
    <div class="favicon-container">
        <a href="${pageContext.request.contextPath}/">
            <img src="img/favicon.png"/>
        </a>
    </div>
    <div class="login-register-user">
        <div id="login-register-container" class="login-register-container">
            <a class="login-register-link" href="login_register.jsp">Đăng kí/Đăng nhập</a>
        </div>
        <div id="user-container" class="user-container" style="display: none">
            Xin chào, <span id="username-span"></span>
            <a onclick="logout();">Đăng xuất</a>
        </div>
    </div>
</header>
<section class="main-content">
    <span id="home"><a href="index.jsp">Trang chủ</a></span>
    <section id="admin_content">
        <div class="crawling_controller_div">
            <span id="controller-label"><b>Điều khiểu bộ cào:</b></span>
            <span id="crawler-status-span">Trạng thái bộ cào: <b><span id="crawler-status"></span></b><span
                    id="delay"></span> </span>
            <button class="control-btn" id="btn-start" onclick="startCrawler();">Bắt đầu</button>
            <button class="control-btn" id="btn-stop" onclick="stopCrawler();">Dừng</button>
        </div>
    </section>
</section>

</body>
<footer>
    CourseX - Nơi tìm kiếm các khóa học online<br>
    <%--Made with 💖 by <a href="https://github.com/TinhHuynh" target="_blank">Tinh Huynh</a>--%>
</footer>
</html>
