package crawler;

public interface ICrawler {
    <T> T fetch();
    <T> T parse();
}
