package crawler.unica;

public enum UnicaCategory {

    CONG_NGHE_THONG_TIN("Công nghệ thông tin"),
    THIET_KE_NHIEP_ANH("Thiết kế - Nhiếp ảnh"),
    SALE_MARKETING("Sale - Marketing"),
    KINH_DOANH_KHOI_NGHIEP("Kinh doanh - Khởi nghiệp"),
    PHAT_TRIEN_CA_NHAN("Phát triển cá nhân"),
    SUC_KHOE_DOI_SONG("Sức khỏe - Giới tính"),
    NGHE_THUAT_LAM_DEP("Nghệ thuật - Làm đẹp"),
    NGOAI_NGU("Ngoại ngữ"),
    NUOI_DAY_CON("Nuôi dạy con");

    private final String value;

    UnicaCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnicaCategory fromValue(String v) {
        for (UnicaCategory c : UnicaCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }


}
