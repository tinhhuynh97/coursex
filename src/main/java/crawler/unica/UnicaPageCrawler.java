package crawler.unica;

import config.Config;
import crawler.BaseCrawler;
import model.category.Category;
import model.course.Course;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.ServletContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class UnicaPageCrawler extends BaseCrawler {

    private Category category;


    public UnicaPageCrawler(ServletContext context, Category category, String cateUrl) {
        super(context, cateUrl);
        this.category = category;
    }

    @Override
    public String fetch() {
        LogUtils.info(UnicaPageCrawler.class.getName(), "Parsing courses from " + url + "...");
        try {
            BufferedReader br = getBufferedReaderFromUrl(url);
            String inputLine, content = "";
            while ((inputLine = br.readLine()) != null) {
                if (inputLine.contains("<div class=\"container list-projects\">")) {
                    content = inputLine;
                    break;
                }
            }
            content = formatHTMLFragment(content);
            content = "<html>" + content + "</html>";
            br.close();
            return content;
        } catch (IOException e) {
            Logger.getLogger(UnicaPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    protected String formatHTMLFragment(String content) {
        int index = content.indexOf("<div class=\"container list-projects\">");
        content = content.substring(index);
        index = content.indexOf("<script type=\"text/javascript\" src=\"https://static.unica.vn/media/js_v2017/slick.js\">");
        content = content.substring(0, index);
        content = content.replaceAll("(.jpg\">)(</img>)?", ".jpg\"/>");
        content = content.replaceAll("(.png\">)(</img>)?", ".png\"/>");
        content = content.replaceAll("(.jpeg\">)(</img>)?", ".jpeg\"/>");
        content = content.replace("&", "&amp;");
        return content.trim();
    }

    @Override
    public Void parse() {
        XMLEventReader reader;
        String content = fetch();
        Course course = new Course();
        List<String> nextPageUrls = new ArrayList<>();
        boolean firstPage = false;
        try {
            reader = XMLUtils.getEventReader(content);
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    switch (element.getName().toString()) {
                        case "div": {
                            boolean isColDiv = XMLUtils.hasAttributeValue(element, "class", "col");
                            if (isColDiv) {
                                course = new Course();
                            }
                            break;
                        }
                        case "a": {
                            String title = XMLUtils.getAttributeValue(element, "title");
                            if (title != null) {
                                course.setName(title);
                            }
                            String link = XMLUtils.getAttributeValue(element, "href");
                            if (link != null) {
                                if (course.getUrl() == null)
                                    course.setUrl(link);
                                if (link.contains("teacher")) {
                                    String lecturer = XMLUtils.getNextString(reader);
                                    course.setLecturerName(lecturer);
                                }
                                if (link.contains("page=")) {
                                    int dataPage = Integer.valueOf(XMLUtils.getAttributeValue(element, "data-page"));
                                    String nextPageUrl = Config.UNICA_URL + link;
                                    if (dataPage >= 1 && !nextPageUrls.contains(nextPageUrl)) {
                                        nextPageUrls.add(nextPageUrl);
                                    }
                                }
                            }
                            break;
                        }
                        case "img": {
                            String imgUrl = XMLUtils.getAttributeValue(element, "src");
                            if (imgUrl != null) {
                                course.setImg(imgUrl);
                            }
                            break;
                        }
                        case "span": {
                            String priceInfoAttr = XMLUtils.getAttributeValue(element, "class");
                            if (priceInfoAttr != null) {
                                switch (priceInfoAttr) {
                                    case "sell-price":
                                        String priceStr = XMLUtils.getNextString(reader);
                                        if (priceStr.equals("Miễn phí")) {
                                            priceStr = "0";
                                        }
                                        course.setSellPrice(Long.valueOf(priceStr.replaceAll("([\",])", "")));
                                        saveCourseToDatabase(course, category);
                                        LogUtils.info(UnicaPageCrawler.class.getName(), "Parsed a course from " + url + "\n" + course.toString());
                                        break;
                                }
                            }
                            break;
                        }
                        case "li": {
                            if (XMLUtils.hasAttributeValue(element, "class", "active")) {
                                element = reader.nextEvent().asStartElement();
                                String link = XMLUtils.getAttributeValue(element, "href");
                                if (link.contains("page=")) {
                                    int dataPage = Integer.valueOf(XMLUtils.getAttributeValue(element, "data-page"));
                                    if (dataPage == 0) {
                                        firstPage = true;
                                    }
                                }
                            }
                            break;
                        }

                    }
                }
            }
            if (firstPage) {
                parseNextPages(nextPageUrls);
            }
        } catch (XMLStreamException e) {
            Logger.getLogger(UnicaPageCrawler.class.getName()).log(Level.SEVERE, url, e);
        }
        return null;
    }

    private void parseNextPages(List<String> urls) {
        urls.forEach(url -> {
            Thread pageThread = new Thread(new UnicaPageCrawler(context, category, url));
            pageThread.start();
        });
    }
}
