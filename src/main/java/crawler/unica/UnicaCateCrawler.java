package crawler.unica;

import crawler.BaseCrawler;
import util.XMLUtils;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class UnicaCateCrawler extends BaseCrawler {

    public UnicaCateCrawler(String url) {
        super(url);
    }

    @Override
    public String fetch() {
        try {
            BufferedReader br = getBufferedReaderFromUrl(url);
            String content = "", inputLine;
            while ((inputLine = br.readLine()) != null) {
                if (inputLine.contains("<ul class=\"dropdown-menu\">")) {
                    content = inputLine;
                    break;
                }
            }
            content = formatHTMLFragment(content);
            content = "<html>" + content + "</html>";
            br.close();
            return content;
        } catch (IOException ex) {
            Logger.getLogger(UnicaCateCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    protected String formatHTMLFragment(String content) {
        System.out.println(content);
        int index = content.indexOf("<ul class=\"dropdown-menu\">");
        content = content.substring(index);
        index = content.indexOf("</li><li><form class=\"navbar-form form-inline\"");
        content = content.substring(0, index);
        return content.trim();
    }

    @Override
    public Map<String, String> parse() {
        XMLEventReader reader;
        Map<String, String> categories = new HashMap<>();

        try {
            String content = fetch();
            reader = XMLUtils.getEventReader(content);
            String cateName, cateUrl = "";
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    String href = XMLUtils.getAttributeValue(element, "href");
                    if (href != null) {
                        cateUrl = href;
                    }
                } else if (event.isCharacters()) {
                    String str = event.asCharacters().toString().replace("\"", "").trim();
                    if (!str.isEmpty()) {
                        cateName = str;
                        categories.put(cateName, cateUrl);
                    }
                    cateName = "";
                    cateUrl = "";
                }
            }
        } catch (XMLStreamException ex) {
            Logger.getLogger(UnicaCateCrawler.class.getName()).log(Level.SEVERE, url, ex);
        }
        return categories;
    }

}
