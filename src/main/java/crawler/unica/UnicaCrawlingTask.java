package crawler.unica;

import config.AppCache;
import config.Config;
import model.category.Category;
import model.category.dictionary.Dictionary;
import util.LogUtils;

import javax.servlet.ServletContext;
import java.util.Map;

public class UnicaCrawlingTask extends Thread implements Runnable {

    private ServletContext context;

    public UnicaCrawlingTask(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        UnicaCateCrawler cateCrawler = new UnicaCateCrawler(Config.UNICA_URL);
        Map<String, String> categories = cateCrawler.parse();
        Dictionary dictionary = AppCache.dictionary;
        categories.forEach((cateName, cateUrl) -> {
            Category category = dictionary.lookUp(cateName);
            if (category != null) {
                LogUtils.info(UnicaCrawlingTask.class.getName(), cateName + " - " + cateUrl);
                Thread pageThread = new Thread(new UnicaPageCrawler(context, category, cateUrl));
                pageThread.start();
            }
        });
    }
}
