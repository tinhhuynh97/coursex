package crawler;

import config.AppCache;
import config.Config;
import dao.category.CategoryDAO;
import dao.course.CourseDAO;
import model.category.Category;
import model.course.Course;
import util.LogUtils;
import util.NetworkUtils;
import util.XMLUtils;

import javax.servlet.ServletContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public abstract class BaseCrawler implements ICrawler, Runnable {

    protected abstract String formatHTMLFragment(String fragment);

    protected String url;
    protected ServletContext context;

    public BaseCrawler(String url) {
        this.url = url;
    }

    public BaseCrawler(ServletContext context, String url) {
        this.context = context;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ServletContext getContext() {
        return context;
    }

    public void setContext(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        parse();
    }

    protected BufferedReader getBufferedReaderFromUrl(String url) throws IOException {
        return NetworkUtils.getBufferedReaderFromUrl(url);
    }

    protected synchronized void saveCourseToDatabase(Course course, Category category) {
        CourseDAO courseDAO = CourseDAO.getInstance();

        if (category != null) {
            Course persistedCourse = courseDAO.findByUrlSync(course.getUrl());
            compareCoursesAndUpdateDB(persistedCourse, course, category);
        }

    }

    private void compareCoursesAndUpdateDB(Course persisted, Course crawled, Category category) {
        String contextPath = context.getRealPath("/");
        String courseXsdPath = contextPath + Config.COURSE_XSD_PATH;

        CourseDAO courseDAO = CourseDAO.getInstance();

        if (persisted == null) {
            if (XMLUtils.validateJAXBObject(crawled, courseXsdPath)) {
                LogUtils.info(BaseCrawler.class.getName(), "inserting " + crawled.toString());
                persisted = crawled;
                addSupplierToCourse(persisted);
                persisted = courseDAO.saveSync(persisted);
                addCategoryToCourse(persisted, category);
            }
        } else {
            if (!persisted.equals(crawled)) {
                persisted.from(crawled);
                if (XMLUtils.validateJAXBObject(persisted, courseXsdPath)) {
                    LogUtils.info(BaseCrawler.class.getName(), "updating " + persisted.toString());
                    courseDAO.updateSync(persisted);
                    addCategoryToCourse(persisted, category);
                }
            } else {
                addCategoryToCourse(persisted, category);
            }
        }

    }

    private void addSupplierToCourse(Course course) {
        AppCache.suppliers.stream()
                .filter(s -> course.getUrl().contains(s.getSupplierUrl()))
                .findFirst()
                .ifPresent(course::setSupplier);
    }

    private void addCategoryToCourse(Course course, Category category) {
        CourseDAO courseDAO = CourseDAO.getInstance();
        CategoryDAO categoryDAO = CategoryDAO.getInstance();

        List<Category> categories = categoryDAO.findByCourseIdSync(course.getId());
        boolean alreadyHasSuchCategory = categories.stream()
                .anyMatch(c -> c.getId().equals(category.getId()));
        if (!alreadyHasSuchCategory) {
            courseDAO.addCategorySync(course.getId(), category.getId());
        }
    }

}
