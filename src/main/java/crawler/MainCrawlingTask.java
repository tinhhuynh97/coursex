package crawler;

import config.Config;
import config.CrawlerStatus;
import crawler.edumall.EdumallCrawlingTask;
import crawler.unica.UnicaCrawlingTask;

import javax.servlet.ServletContext;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MainCrawlingTask extends Thread {
    private static final Object LOCK = new Object();
    private static MainCrawlingTask INSTANCE;
    private ServletContext context;
    private CrawlerStatus status = CrawlerStatus.IDLING;
    private ScheduledExecutorService scheduler;
    private ScheduledFuture scheduledFuture;
    private boolean running = false;

    private MainCrawlingTask() {
    }

    private MainCrawlingTask(ServletContext context) {
        this.context = context;
    }

    public static MainCrawlingTask getInstance(ServletContext servletContext) {
        synchronized (LOCK) {
            if (INSTANCE == null) {
                INSTANCE = new MainCrawlingTask(servletContext);
            }
        }
        return INSTANCE;
    }

    @Override
    public void run() {
        crawlData();
    }

    public void startTask() {
        if (!running && (scheduler == null || scheduler.isShutdown())) {
            running = true;
            scheduler = Executors.newScheduledThreadPool(1);
            scheduledFuture = scheduler.scheduleAtFixedRate(this, 0, Config.CRAWLING_INTERVAL_IN_DAY, TimeUnit.DAYS);
            setStatus(CrawlerStatus.WORKING);
        }
    }


    public void stopTask() {
        if (running && scheduler != null && !scheduler.isShutdown()) {
            running = false;
            scheduledFuture.cancel(true);
            scheduler.shutdown();
            setStatus(CrawlerStatus.TERMINATED);
        }
    }

    private void crawlData() {
        UnicaCrawlingTask unicaCrawlingTask = new UnicaCrawlingTask(context);
        unicaCrawlingTask.start();
        EdumallCrawlingTask edumallCrawlingTask = new EdumallCrawlingTask(context);
        edumallCrawlingTask.start();
    }

    public int getDelayForNextCrawl() {
        if (scheduledFuture != null) {
            return (int) scheduledFuture.getDelay(TimeUnit.SECONDS);
        }
        return -1;
    }

    public CrawlerStatus getStatus() {
        return status;
    }

    public void setStatus(CrawlerStatus status) {
        this.status = status;
    }
}
