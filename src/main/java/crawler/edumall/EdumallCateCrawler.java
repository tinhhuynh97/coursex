package crawler.edumall;

import config.Config;
import crawler.BaseCrawler;
import crawler.unica.UnicaCateCrawler;
import util.XMLUtils;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class EdumallCateCrawler extends BaseCrawler {

    public EdumallCateCrawler(String url) {
        super(url);
    }

    @Override
    public String fetch() {
        try {
            BufferedReader br = getBufferedReaderFromUrl(url);
            StringBuilder content = new StringBuilder();
            String inputLine;
            boolean foundCateHTMLCode = false;
            while ((inputLine = br.readLine()) != null) {
                if (inputLine.contains("<div class='col-xs col-sm col-md col-lg main-header-v4--content-c-header-left'>")) {
                    foundCateHTMLCode = true;
                    content.append(inputLine);
                } else {
                    if (foundCateHTMLCode) {
                        if (inputLine.contains("<div class='col-xs col-sm col-md col-lg main-header-v4--content-c-header-search'>")) {
                            break;
                        } else {
                            content.append(inputLine);
                        }
                    }
                }
            }
            content = new StringBuilder(formatHTMLFragment(content.toString()));
            content = new StringBuilder("<html>" + content + "</html>");
            br.close();
            return content.toString();
        } catch (IOException ex) {
            Logger.getLogger(EdumallCateCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    protected String formatHTMLFragment(String content) {
        content = content.trim();
        content = content.replace("#9b9b9b'>", "#9b9b9b'/>")
                .replace(".svg'>", ".svg'/>")
                .replace("</i>", "");
        return content;
    }

    @Override
    public Map<String, String> parse() {
        XMLEventReader reader;
        Map<String, String> categories = new HashMap<>();
        try {
            String content = fetch();
            reader = XMLUtils.getEventReader(content);
            String cateName, cateUrl = "";
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    switch (element.getName().toString()) {
                        case "a": {
                            if (XMLUtils.hasAttributeValue(element, "class", "text")) {
                                String categoryUrl = XMLUtils.getAttributeValue(element, "href");
                                if (categoryUrl != null) {
                                    cateUrl = Config.EDUMALL_URL + categoryUrl;
                                }
                            }
                            break;
                        }
                        case "span": {
                            if (XMLUtils.hasAttributeValue(element, "style", "font-size: 13px; " +
                                    "margin-left:10px;line-height: 1.38;letter-spacing: -0.1px;text-align: left;flex:1;" +
                                    "min-width: 1px;margin-right: 0;")) {
                                cateName = XMLUtils.getNextString(reader);
                                categories.put(cateName, cateUrl);
                                cateUrl = "";
                                cateName = "";
                            }
                            break;
                        }
                    }
                }
            }
        } catch (XMLStreamException ex) {
            Logger.getLogger(UnicaCateCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categories;
    }

}
