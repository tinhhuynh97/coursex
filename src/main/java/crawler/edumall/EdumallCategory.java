package crawler.edumall;

public enum EdumallCategory {

    CONG_NGHE_THONG_TIN("Công nghệ thông tin"),
    THIET_KE("Thiết kế"),
    MARKETING("Marketing"),
    KINH_DOANH_KHOI_NGHIEP("Kinh doanh khởi nghiệp"),
    PHAT_TRIEN_CA_NHAN("Phát triển cá nhân"),
    NGOAI_NGU("Ngoại ngữ"),
    NUOI_DAY_CON("Nuôi dạy con"),
    TIN_HOC_VAN_PHONG("Tin học văn phòng"),
    MULTIMEDIA("Multimedia"),
    AM_NHAC("Âm nhạc"),
    LIFESTYLE("Life style"),
    THE_THAO_SUC_KHOE("Thể thao và sức khỏe");

    private final String value;

    EdumallCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EdumallCategory fromValue(String v) {
        for (EdumallCategory c : EdumallCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }


}
