package crawler.edumall;

import config.Config;
import crawler.BaseCrawler;
import crawler.unica.UnicaPageCrawler;
import model.category.Category;
import model.course.Course;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.ServletContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class EdumallPageCrawler extends BaseCrawler {
    private Category category;


    public EdumallPageCrawler(ServletContext context, Category category, String cateUrl) {
        super(context, cateUrl);
        this.category = category;
    }

    @Override
    public String fetch() {
        LogUtils.info(EdumallPageCrawler.class.getName(), "Parsing courses from " + url + "...");
        try {
            BufferedReader br = getBufferedReaderFromUrl(url);
            String inputLine;
            StringBuilder content = new StringBuilder();
            boolean foundCoursesHtml = false;
            while ((inputLine = br.readLine()) != null) {
                if (inputLine.contains("<section class='area-display-courses'>")) {
                    foundCoursesHtml = true;
                    content.append(inputLine);
                } else if (foundCoursesHtml) {
                    content.append(inputLine);
                    if (inputLine.contains("</section>")) {
                        break;
                    }
                }
            }
            content = new StringBuilder(formatHTMLFragment(content.toString()));
            content = new StringBuilder("<html>" + content + "</html>");
            br.close();
            return content.toString();
        } catch (IOException e) {
            Logger.getLogger(UnicaPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }


    @Override
    public Void parse() {
        XMLEventReader reader;
        String content = fetch();
        Course course = new Course();
        boolean addCourse = false;
        boolean firstPage = false;
        List<String> nextPageUrls = new ArrayList<>();
        try {
            reader = XMLUtils.getEventReader(content);
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = event.asStartElement();
                    switch (element.getName().getLocalPart()) {
                        case "a": {
                            String link = Config.EDUMALL_URL + XMLUtils.getAttributeValue(element, "href");
                            if (XMLUtils.hasAttributeValue(element, "class", "gtm_section_recommendation")) {
                                if (course.getUrl() == null) {
                                    course.setUrl(link);
                                }
                            } else if (link.contains("page=") && !nextPageUrls.contains(link)) {
                                nextPageUrls.add(link);
                            }
                            break;
                        }
                        case "div": {
                            if (XMLUtils.hasAttributeValue(element, "class", "course-header img-thumb gtm_section_recommendation")) {
                                if (course.getImg() == null) {
                                    String img = XMLUtils.getAttributeValue(element, "data-src");
                                    course.setImg(img);
                                }
                            }
                            break;
                        }
                        case "h5": {
                            if (XMLUtils.hasAttributeValue(element, "class", "gtm_section_recommendation course-title")) {
                                String courseName = XMLUtils.getNextString(reader);
                                course.setName(courseName);
                            }
                            break;
                        }
                        case "span": {
                            if (XMLUtils.hasAttributeValue(element, "class", "gtm_section_recommendation")) {
                                String lecturer = XMLUtils.getNextString(reader);
                                course.setLecturerName(lecturer);
                            } else if (XMLUtils.hasAttributeValue(element, "class", "gtm_section_recommendation new-price sale-off")) {
                                String priceStr = XMLUtils.getNextString(reader);
                                priceStr = priceStr.replaceAll("([,đ])", "");
                                course.setSellPrice(Long.parseLong(priceStr));
                                addCourse = true;
                            } else if (XMLUtils.hasAttributeValue(element, "class", "gtm_section_recommendation new-price")) {
                                if (XMLUtils.getNextString(reader).equals("Miễn phí")) {
                                    Long price = Long.valueOf("0");
                                    course.setSellPrice(price);
                                    addCourse = true;
                                }
                            }
                            break;
                        }
                        case "em": {
                            if (XMLUtils.hasAttributeValue(element, "class", "current")) {
                                int pageNum = Integer.parseInt(XMLUtils.getNextString(reader));
                                if (pageNum == 1) {
                                    firstPage = true;
                                }
                            }
                            break;
                        }

                    }
                    if (addCourse) {
                        LogUtils.info(EdumallPageCrawler.class.getName(), "Parsed a course from " + url + "\n" + course.toString());
                        saveCourseToDatabase(course, category);
                        course = new Course();
                        addCourse = false;

                    }
                }
            }
            if (firstPage) {
                parseNextPages(nextPageUrls);
            }
        } catch (XMLStreamException e) {
            Logger.getLogger(EdumallPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    private void parseNextPages(List<String> urls) {
        urls.forEach(url -> {
            Thread pageThread = new Thread(new EdumallPageCrawler(context, category, url));
            pageThread.start();
        });
    }


    @Override
    protected String formatHTMLFragment(String content) {
        content = content.replace("type='number'>", "type='number'/>");
        return content.trim();
    }
}
