package crawler.edumall;

import config.AppCache;
import config.Config;
import model.category.Category;
import model.category.dictionary.Dictionary;
import util.LogUtils;

import javax.servlet.ServletContext;
import java.util.Map;

public class EdumallCrawlingTask extends Thread implements Runnable {

    private ServletContext context;

    public EdumallCrawlingTask(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        EdumallCateCrawler cateCrawler = new EdumallCateCrawler(Config.EDUMALL_URL);
        Map<String, String> categories = cateCrawler.parse();
        Dictionary dictionary = AppCache.dictionary;
        categories.forEach((cateName, cateUrl) -> {
            Category category = dictionary.lookUp(cateName);
            if (category != null) {
                LogUtils.info(EdumallCrawlingTask.class.getName(), cateName + " - " + cateUrl);
                Thread pageThread = new Thread(new EdumallPageCrawler(context, category, cateUrl));
                pageThread.start();
            }
        });
    }
}
