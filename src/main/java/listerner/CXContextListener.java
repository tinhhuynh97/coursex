package listerner;

import config.AppCache;
import util.HibernateUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@javax.servlet.annotation.WebListener()
public class CXContextListener implements ServletContextListener {

    // Public constructor is required by servlet spec
    public CXContextListener() {
    }

    // -------------------------------------------------------
    // CXContextListener implementation
    // -------------------------------------------------------
    @Override
    public void contextInitialized(ServletContextEvent sce) {
      /* This method is called when the servlet context is
         initialized(when the Web application is deployed). 
         You can initialize servlet context related model here.
      */
        HibernateUtils.getSessionFactory();
        AppCache.setUpCache();
    }


    @Override
    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         Application Server shuts down.
      */
    }
}
