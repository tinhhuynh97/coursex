package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@SuppressWarnings("unchecked")
public abstract class BaseDAO<T, Id extends Serializable> implements IDAO<T, Id>  {
    private Class<T> clazz;
    protected Session currentSession;
    protected Transaction currentTransaction;

    public BaseDAO() {
        this.clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void persist(T entity) {
            openCurrentSessionwithTransaction();
            getCurrentSession().persist(entity);
            closeCurrentSessionwithTransaction();
    }


    @Override
    public void persistAll(List<T> entities) {
        entities.forEach(this::persist);
    }

    @Override
    public void update(T entity) {
        openCurrentSessionwithTransaction();
        getCurrentSession().merge(entity);
        closeCurrentSessionwithTransaction();
    }

    @Override
    public T findById(Id id) {
        openCurrentSession();
        T obj = getCurrentSession().get(clazz, id);
        closeCurrentSession();
        return obj;
    }

    @Override
    public void delete(T entity) {
        openCurrentSessionwithTransaction();
        getCurrentSession().delete(entity);
        closeCurrentSessionwithTransaction();
    }

    @Override
    public List<T> findAll() {
        openCurrentSession();
        List<T> list = (List<T>) getCurrentSession()
                .createQuery("from " + clazz.getName()).list();
        closeCurrentSession();
        return list;
    }

    @Override
    public void deleteAll() {
        List<T> entityList = findAll();
        entityList.forEach(this::delete);
    }

    public Session openCurrentSession() {
        currentSession = HibernateUtils.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = HibernateUtils.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }


    public void closeCurrentSession() {
        if(currentSession != null) {
            currentSession.close();
        }
    }

    public void closeCurrentSessionwithTransaction() {
        if(currentSession != null && currentTransaction != null) {
            currentTransaction.commit();
            currentSession.close();
        }
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }


    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

}
