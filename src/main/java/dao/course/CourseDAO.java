package dao.course;

import config.Options;
import dao.BaseDAO;
import model.course.Course;
import util.LogUtils;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.math.BigInteger;
import java.util.List;

@SuppressWarnings("unchecked")
public class CourseDAO extends BaseDAO<Course, Integer> {

    private static CourseDAO INSTANCE;
    private static final Object LOCK = new Object();

    public static CourseDAO getInstance() {
        synchronized (LOCK) {
            if (INSTANCE == null) {
                INSTANCE = new CourseDAO();
            }
        }
        return INSTANCE;
    }

    public synchronized void persist(Course course) {
        persist(course);
    }

    public synchronized Course saveSync(Course course) {
        return save(course);
    }


    public Course save(Course entity) {
        openCurrentSessionwithTransaction();
        getCurrentSession().save(entity);
        closeCurrentSessionwithTransaction();
        return entity;
    }

    public synchronized void persistAllSync(List<Course> courses) {
        courses.forEach(this::persist);
    }

    public synchronized Course findByUrlSync(String url) {
        Course course;
        course = findByUrl(url);
        return course;
    }

    public synchronized void updateSync(Course course) {
        update(course);
    }

    public synchronized void addCategorySync(Integer courseId, Integer categoryId) {
        String sql = "INSERT INTO course_category (course_id, category_id) VALUES (?1, ?2)";
        openCurrentSessionwithTransaction();
        getCurrentSession()
                .createNativeQuery(sql)
                .setParameter(1, courseId)
                .setParameter(2, categoryId)
                .executeUpdate();
        closeCurrentSessionwithTransaction();
    }


    public Course findByUrl(String url) {
        Course course = null;
        try {
            openCurrentSession();
            course = (Course) getCurrentSession()
                    .createQuery("from Course where url = :url")
                    .setParameter("url", url)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException ignored) {
        } catch (NonUniqueResultException e) {
            LogUtils.error(CourseDAO.class.getName(), "", e);
        } finally {
            closeCurrentSession();
        }
        return course;
    }

    public List<Course> findAll(Integer limit, Integer offset, Options options) {
        String hql = "from Course";
        hql = updateHqlWithOptions(hql, options, true);
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createQuery(hql)
                .setMaxResults(limit)
                .setFirstResult(offset)
                .getResultList();
        closeCurrentSession();
        return courses;
    }


    public List<Course> findAllByCategory(Integer cateId, Integer limit, Integer offset, Options options) {
        String sql = "select course.* from course join course_category c on course.id = c.course_id " +
                "where c.category_id = ?1 ";
        sql = updateSqlWithOptions(sql, options, false);
        sql += " limit ?2 offset ?3";
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createNativeQuery(sql, Course.class)
                .setParameter(1, cateId)
                .setParameter(2, limit)
                .setParameter(3, offset)
                .getResultList();
        closeCurrentSession();
        return courses;
    }

    public List<Course> findBySearch(String search, int limit, int offset, Options options) {
        String sql = "select distinct * from course where (course_name like :search or lecturer_name like :search) ";
        sql = updateSqlWithOptions(sql, options, false);
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createNativeQuery(sql, Course.class)
                .setParameter("search", "%" + search + "%")
                .setMaxResults(limit)
                .setFirstResult(offset)
                .getResultList();
        closeCurrentSession();
        return courses;
    }

    public List<Course> findFavoriteByUserId(int userId, int limit, int offset, Options options) {
        String sql = "select distinct course.* from course join user_favorite_course u on course.id = u.course_id where user_id = :userId and isActive = true ";
        sql = updateSqlWithOptions(sql, options, false);
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createNativeQuery(sql, Course.class)
                .setParameter("userId", userId)
                .setMaxResults(limit)
                .setFirstResult(offset)
                .getResultList();
        closeCurrentSession();
        return courses;
    }

    public List<Course> findAllFavorite() {
        String sql = "select DISTINCT course.* from course join user_favorite_course u on course.id = u.course_id " +
                "and isActive = true";
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createNativeQuery(sql, Course.class)
                .getResultList();
        closeCurrentSession();
        return courses;
    }

    public int getFavoriteCount() {
        int count;
        String sql = "select count(distinct course_id) from user_favorite_course join course c on " +
                "user_favorite_course.course_id = c.id where isActive = true";
        openCurrentSession();
        count = (int) getCurrentSession()
                .createNativeQuery(sql)
                .getSingleResult();
        closeCurrentSession();
        return count;
    }

    public int getFavoriteCount(int courseId) {
        int count;
        String sql = "select count(distinct course_id) from user_favorite_course join course c on " +
                "user_favorite_course.course_id = c.id where course_id = :courseId and isActive = true";
        openCurrentSession();
        count = (int) getCurrentSession()
                .createNativeQuery(sql)
                .setParameter("courseid", courseId)
                .getSingleResult();
        closeCurrentSession();
        return count;
    }


    public List<Course> findFavoriteByUserId(int userId) {
        String sql = "select distinct course.* from course join user_favorite_course u on course.id = u.course_id " +
                "where user_id = :userId and isActive = true";
        List<Course> courses;
        openCurrentSession();
        courses = getCurrentSession()
                .createNativeQuery(sql, Course.class)
                .setParameter("userId", userId)
                .getResultList();
        closeCurrentSession();
        return courses;
    }

    public int getCount(String nextContentSql, Options options, boolean needWhereForOptions) {
        String sql;
        Integer count;
        sql = String.format("SELECT COUNT(distinct course.id) FROM course %s ", nextContentSql);
        sql = updateSqlWithOptions(sql, options, needWhereForOptions);
        openCurrentSession();
        count = ((BigInteger) getCurrentSession()
                .createNativeQuery(sql)
                .getSingleResult()).intValue();
        closeCurrentSession();
        return count;

    }

    public Integer getCountAll(Options options) {
        return getCount("", options, true);
    }

    public Integer getCountAllByCategory(int cateId, Options options) {
        return getCount(" join course_category c on course.id = c.course_id " +
                " where c.category_id = " + cateId, options, false);
    }

    public Integer getCountSearch(String search, Options options) {
        return getCount(" where (course.course_name like '%" + search +
                "%' or lecturer_name like '%" + search + "%')", options, false);
    }

    public Integer getCountFavoriteByUserId(int userId, Options options) {
        return getCount(" join user_favorite_course u on course.id = u.course_id where user_id = "
                + userId + " and isActive = true ", options, false);
    }

    private String updateSqlWithOptions(String sql, Options options, boolean needWhereForOptions) {
        if (options != null) {
            if (options.getMinPrice() != null) {
                if (needWhereForOptions) {
                    sql += String.format(" where sell_price >= %d ", options.getMinPrice());
                } else {
                    sql += String.format(" and sell_price >= %d ", options.getMinPrice());
                }
                needWhereForOptions = false;
            }
            if (options.getMaxPrice() != null) {
                if (needWhereForOptions) {
                    sql += String.format(" where sell_price <= %d ", options.getMaxPrice());
                } else {
                    sql += String.format(" and sell_price <= %d ", options.getMaxPrice());
                }
            }
            if (options.getSortCriteria() != null) {
                switch (options.getSortCriteria()) {
                    case PRICE_ASC:
                        sql += " order by sell_price asc ";
                        break;
                    case PRICE_DESC:
                        sql += " order by sell_price desc ";
                        break;
                }
            }
        }
        return sql.trim();
    }

    private String updateHqlWithOptions(String hql, Options options, boolean needWhereForOptions) {
        if (options != null) {
            if (options.getMinPrice() != null) {
                if (needWhereForOptions) {
                    hql += String.format(" where sellPrice >= %d ", options.getMinPrice());

                } else {
                    hql += String.format(" and sellPrice >= %d ", options.getMinPrice());

                }
                needWhereForOptions = false;
            }
            if (options.getMaxPrice() != null) {
                if (needWhereForOptions) {
                    hql += String.format(" where sellPrice <= %d ", options.getMaxPrice());
                } else {
                    hql += String.format(" and sellPrice <= %d ", options.getMaxPrice());
                }
            }
            if (options.getSortCriteria() != null) {
                switch (options.getSortCriteria()) {
                    case PRICE_ASC:
                        hql += " order by sellPrice asc";
                        break;
                    case PRICE_DESC:
                        hql += " order by sellPrice desc";
                        break;
                }
            }
        }
        return hql.trim();
    }


}
