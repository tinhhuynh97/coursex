package dao;

import model.supplier.Supplier;

import javax.persistence.NoResultException;

public class SupplierDAO extends BaseDAO<Supplier, Integer> {

    public Supplier findByLikeName(String name) {
        Supplier supplier;
        openCurrentSession();
        try {
            supplier = (Supplier) getCurrentSession().createQuery("from Supplier where supplierName like :name")
                    .setParameter("name", name)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            supplier = null;
        }
        return supplier;
    }

    public synchronized Supplier findByLikeNameSync(String name) {
        return findByLikeName(name);
    }

    public Supplier findByCourseId(int id) {
        Supplier supplier;
        openCurrentSession();
        try {
            supplier = (Supplier) getCurrentSession().createQuery("from Supplier where id = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            supplier = null;
        }
        return supplier;
    }

}
