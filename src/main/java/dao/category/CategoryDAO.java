package dao.category;

import dao.BaseDAO;
import model.category.Category;

import javax.persistence.NoResultException;
import java.util.List;

public class CategoryDAO extends BaseDAO<Category, Integer> {
    private static CategoryDAO INSTANCE;
    private static final Object LOCK = new Object();

    public static CategoryDAO getInstance() {
        synchronized (LOCK) {
            if (INSTANCE == null) {
                INSTANCE = new CategoryDAO();
            }
        }
        return INSTANCE;
    }

    public synchronized Category findByNameSync(String name) {
        Category category;
        category = findByName(name);
        return category;
    }

    public synchronized List<Category> findByCourseIdSync(Integer id) {
        return findByCourseId(id);
    }

    public synchronized List<Category> findByCourseUrlSync(String url) {
        return findByCourseUrl(url);
    }

    public Category findByName(String name) {
        Category category;
        try {
            openCurrentSession();
            category = (Category) getCurrentSession().createQuery("from Category where name like :name")
                    .setParameter("name", name)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            category = null;
        } finally {
            closeCurrentSession();
        }
        return category;
    }

    public List<Category> findByCourseId(Integer id) {
        List<Category> categories;
        String sql = "SELECT ca.id, ca.cate_name, ca.description from course_category JOIN category ca on " +
                "course_category.category_id = ca.id where course_id = ?1";
        openCurrentSession();
        categories = getCurrentSession()
                .createNativeQuery(sql, Category.class)
                .setParameter(1, id)
                .getResultList();
        closeCurrentSession();
        return categories;
    }

    public List<Category> findByCourseUrl(String url) {
        List<Category> categories;
        String sql = "SELECT ca.id, ca.cate_name, ca.description from course_category JOIN category ca on " +
                "course_category.category_id = ca.id join course co on " +
                "course_category.course_id = co.id where co.url = ?1";
        openCurrentSession();
        categories = getCurrentSession()
                .createNativeQuery(sql, Category.class)
                .setParameter(1, url)
                .getResultList();
        closeCurrentSession();
        return categories;
    }

    public String getDictionaryWordsById(int cateId) {
        String words;
        String sql = "SELECT words from category_dictionary where category_id = :cateId";
        openCurrentSession();
        words = (String) getCurrentSession()
                .createNativeQuery(sql)
                .setParameter("cateId", cateId)
                .getSingleResult();
        closeCurrentSession();
        return words;
    }


}
