package dao.user;

import config.Role;
import dao.BaseDAO;
import model.user.User;
import util.LogUtils;

import javax.persistence.NoResultException;
import java.math.BigInteger;
import java.util.List;

public class UserDAO extends BaseDAO<User, Integer> {
    public User authenticate(String email, String password) {
        User user = null;
        try {
            openCurrentSession();
            user = (User) getCurrentSession()
                    .createQuery("from User where email = :email and password = :password")
                    .setParameter("email", email)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException e) {
            LogUtils.info(UserDAO.class.getName(), "no user with " + email);
        } finally {
            closeCurrentSession();
        }
        return user;
    }

    public boolean checkAdminByEmail(String email) {
        boolean isAdmin;
        User user;
        try {
            openCurrentSession();
            user = (User) getCurrentSession()
                    .createQuery("from User where email = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            isAdmin = user.getRole().equals(Role.ADMIN.toString());
        } catch (NoResultException e) {
            isAdmin = false;
            LogUtils.info(UserDAO.class.getName(), "no user with " + email);
        } finally {
            closeCurrentSession();
        }
        return isAdmin;
    }

    public User getByEmail(String email) {
        User user;
        try {
            openCurrentSession();
            user = (User) getCurrentSession()
                    .createQuery("from User where email = :email")
                    .setParameter("email", email)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            user = null;
            LogUtils.info(UserDAO.class.getName(), "no user with " + email);
        } finally {
            closeCurrentSession();
        }
        return user;
    }

    public int checkExistingFavoriteCourse(int userId, int courseId) {
        int id;
        try {
            openCurrentSession();
            id = (int) getCurrentSession()
                    .createNativeQuery("select id from user_favorite_course where user_id = :userId " +
                            "and course_id = :courseId")
                    .setParameter("userId", userId)
                    .setParameter("courseId", courseId)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            id = -1;
        } finally {
            closeCurrentSession();
        }
        return id;
    }

    public boolean checkActiveFavoriteCourse(int favoriteId) {
        boolean active;
        try {
            openCurrentSession();
            active = (boolean) getCurrentSession()
                    .createNativeQuery("select isActive from user_favorite_course where id = :favoriteId")
                    .setParameter("favoriteId", favoriteId)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            active = false;
        } finally {
            closeCurrentSession();
        }
        return active;
    }

    public void addFavoriteCourse(int userId, int courseId) {
        openCurrentSessionwithTransaction();
        getCurrentSession()
                .createNativeQuery("insert into user_favorite_course(user_id, course_id, isActive) " +
                        "values(:userId, :courseId, true)")
                .setParameter("userId", userId)
                .setParameter("courseId", courseId)
                .executeUpdate();
        closeCurrentSessionwithTransaction();
    }

    public void updateFavoriteCourse(int userId, int courseId, boolean isActive) {
        openCurrentSessionwithTransaction();
        getCurrentSession()
                .createNativeQuery("update user_favorite_course " +
                        "set isActive = :isActive where user_id = :userId and course_id = :courseId")
                .setParameter("userId", userId)
                .setParameter("courseId", courseId)
                .setParameter("isActive", isActive)
                .executeUpdate();
        closeCurrentSessionwithTransaction();
    }

    public List<User> findFavoriteByCourseId(int courseId) {
        List<User> users;
        String sql = "select distinct * from user join user_favorite_course u on user.id = u.user_id  " +
                "where course_id = :courseid and isActive = true";
        openCurrentSession();
        users = getCurrentSession()
                .createNativeQuery(sql, User.class)
                .setParameter("courseid", courseId)
                .getResultList();
        closeCurrentSession();
        return users;
    }


    public int getUserFavoriteCount(int courseId) {
        int count;
        String sql = "select count(distinct user_id) from user_favorite_course join course c on " +
                "user_favorite_course.course_id = c.id where course_id = :courseid and isActive = true";
        openCurrentSession();
        count = ((BigInteger) getCurrentSession()
                .createNativeQuery(sql)
                .setParameter("courseid", courseId)
                .getSingleResult()).intValue();
        closeCurrentSession();
        return count;
    }


    public int getUserFavoriteCount() {
        int count;
        String sql = "select count(distinct user_id) from user_favorite_course join course c on " +
                "user_favorite_course.course_id = c.id and isActive = true";
        openCurrentSession();
        count = ((BigInteger) getCurrentSession()
                .createNativeQuery(sql)
                .getSingleResult()).intValue();
        closeCurrentSession();
        return count;
    }
}
