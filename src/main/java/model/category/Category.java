
package model.category;


import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name",
        "description",
//        "courses"
})
@XmlRootElement(name = "Category", namespace = "http://www.coursex.com/xsd/category")
@Table(name = "category")
@Entity
public class Category implements Serializable {

    @XmlElement(name = "CategoryName", namespace = "http://www.coursex.com/xsd/category", required = true)
    protected String name;
    @XmlElement(name = "Description", namespace = "http://www.coursex.com/xsd/category")
    protected String description;
    @XmlAttribute(name = "id", required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected Integer id;

    @Column(name = "cate_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Category) {
            Category other = (Category) obj;
            return this.id.equals(other.id) &&
                    this.name.equals(other.name) &&
                    this.description.equals(other.description);
        }
        return super.equals(obj);
    }
}
