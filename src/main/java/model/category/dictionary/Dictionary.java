package model.category.dictionary;

import model.category.Category;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dictionary {
    private List<Record> records;

    public Dictionary() {
        records = new ArrayList<>();
    }

    public Dictionary(List<Record> records) {
        this.records = records;
    }

    public Category lookUp(String input) {
        Category category = null;
        records.forEach(record -> record.calculateScore(input));
        Collections.sort(records);
        Collections.reverse(records);
        Record highestScoreRecord = records.get(0);
        if (highestScoreRecord.getScore() > 0) {
            category = highestScoreRecord.getCategory();
        }
        return category;
    }

    public void clear() {
        records.clear();
    }

    public void add(Record record) {
        if (!records.contains(record)) {
            records.add(record);
        }
    }

    public void remove(Record record) {
        records.remove(record);
    }
}
