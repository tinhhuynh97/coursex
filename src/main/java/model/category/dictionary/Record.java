package model.category.dictionary;


import model.category.Category;

import java.util.Arrays;
import java.util.Objects;

public class Record implements Comparable<Record> {
    private String[] words;
    private Integer score;
    private Category category;

    public Record() {

    }

    public Record(Category category, String[] words) {
        this.category = category;
        this.words = words;
    }

    public String[] getWords() {
        return words;
    }

    public void setWords(String[] words) {
        this.words = words;
    }

    public Integer getScore() {
        return score;
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public int calculateScore(String input) {
        score = 0;
        for (String word : words) {
            if (input.toLowerCase().contains(word.toLowerCase())) {
                score++;
            }
        }
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Arrays.equals(words, record.words) &&
                Objects.equals(category, record.category);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(category);
        result = 31 * result + Arrays.hashCode(words);
        return result;
    }

    @Override
    public int compareTo(Record o) {
        return this.score.compareTo(o.score);
    }

}
