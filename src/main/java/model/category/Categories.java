package model.category;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "category"
})
@XmlRootElement(name = "Categories", namespace = "http://www.coursex.com/xsd/categories")
public class Categories implements Serializable {

    @XmlElement(name = "Category", namespace = "http://www.coursex.com/xsd/category", required = true)
    protected List<Category> category;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }
}
