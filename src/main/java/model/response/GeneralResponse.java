package model.response;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", namespace = "http://www.coursex.com/xsd/response", propOrder = {
        "code",
        "message"
})
@XmlRootElement(name = "Response", namespace = "http://www.coursex.com/xsd/response")
public class GeneralResponse {
    @XmlElement(name = "Code", namespace = "http://www.coursex.com/xsd/response", required = true)
    protected String code;
    @XmlElement(name = "Message", namespace = "http://www.coursex.com/xsd/response")
    protected String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
