
package model.response;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;


/**
 * <p>Java class for CrawlerResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CrawlerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Delay" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", namespace = "http://www.coursex.com/xsd/crawler_response", propOrder = {
        "status",
        "delay"
})
@XmlRootElement(name = "Response", namespace = "http://www.coursex.com/xsd/crawler_response")
public class CrawlerResponse {

    @XmlElement(name = "Status", namespace = "http://www.coursex.com/xsd/crawler_response", required = true)
    protected String status;
    @XmlElement(name = "Delay", namespace = "http://www.coursex.com/xsd/crawler_response")
    protected Integer delay;

    /**
     * Gets the value of the status property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the delay property.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    public Integer getDelay() {
        return delay;
    }

    /**
     * Sets the value of the delay property.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    public void setDelay(Integer value) {
        this.delay = value;
    }

}
