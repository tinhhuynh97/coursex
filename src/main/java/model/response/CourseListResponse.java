package model.response;


import model.category.Categories;
import model.course.Courses;
import model.pagination.Pages;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "courses",
        "pages"
})
@XmlSeeAlso({Categories.class})
@XmlRootElement(name = "Response", namespace = "http://www.coursex.com/xsd/courseListResponse")
public class CourseListResponse implements Serializable {

    @XmlElement(name = "Courses", namespace = "http://www.coursex.com/xsd/courses")
    protected Courses courses;
    @XmlElement(name = "Pages", namespace = "http://www.coursex.com/xsd/pages")
    protected Pages pages;

    public Courses getCourses() {
        return courses;
    }

    public void setCourses(Courses courses) {
        this.courses = courses;
    }

    public Pages getPages() {
        return pages;
    }

    public void setPages(Pages pages) {
        this.pages = pages;
    }
}
