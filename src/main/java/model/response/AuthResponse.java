package model.response;

import model.user.User;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for CrawlerResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CrawlerResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{http://www.coursex.com/xsd/user}model.user.User" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CrawlerResponse", namespace = "http://www.coursex.com/xsd/auth_response", propOrder = {
        "code",
        "user"
})
@XmlRootElement(name = "Response", namespace = "http://www.coursex.com/xsd/auth_response")
public class AuthResponse {

    @XmlElement(name = "Code", namespace = "http://www.coursex.com/xsd/auth_response", required = true)
    protected String code;
    @XmlElement(name = "User", namespace = "http://www.coursex.com/xsd/user")
    protected User user;

    /**
     * Gets the value of the code property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the user property.
     *
     * @return possible object is
     * {@link User }
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     *
     * @param value allowed object is
     *              {@link User }
     */
    public void setUser(User value) {
        this.user = value;
    }

}
