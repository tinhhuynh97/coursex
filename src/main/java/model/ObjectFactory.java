//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Any modifications to this file will be lost upon recompilation of the source schema.
// Generated on: 2018.06.30 at 05:07:05 PM ICT
//


package model;

import model.category.Categories;
import model.category.Category;
import model.course.Course;
import model.course.Courses;
import model.pagination.Page;
import model.pagination.Pages;
import model.response.AuthResponse;
import model.response.CourseListResponse;
import model.response.CrawlerResponse;
import model.response.GeneralResponse;
import model.supplier.Supplier;
import model.user.User;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the model package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: model
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Pages }
     *
     */
    public Pages createPages() {
        return new Pages();
    }

    /**
     * Create an instance of {@link Page }
     *
     */
    public Page createPage() {
        return new Page();
    }


    public Courses createCourses() {
        return new Courses();
    }

    /**
     * Create an instance of {@link Course }
     *
     */
    public Course createCourse() {
        return new Course();
    }


    public Category createCategory() {
        return new Category();
    }


    public Categories createCategories() {
        return new Categories();
    }

    public CourseListResponse createCourseListResponse(){
        return new CourseListResponse();
    }


    public AuthResponse createAuthResponse() {
        return new AuthResponse();
    }

    public User createUser() {
        return new User();
    }

    public CrawlerResponse createCrawlerResponse() {
        return new CrawlerResponse();
    }

    public Supplier createSupplier() {
        return new Supplier();
    }

    public GeneralResponse createGeneralResponse() {
        return new GeneralResponse();
    }

}
