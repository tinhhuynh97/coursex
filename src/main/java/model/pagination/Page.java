//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.06.30 at 05:07:05 PM ICT 
//


package model.pagination;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OnClick" type="{http://www.coursex.com/xsd/page}OnClick" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="pageNum" type="{http://www.coursex.com/xsd/page}pageNum" default="-1" />
 *       &lt;attribute name="active" type="{http://www.coursex.com/xsd/page}active" default="false" />
 *       &lt;attribute name="previous" type="{http://www.coursex.com/xsd/page}previous" default="false" />
 *       &lt;attribute name="next" type="{http://www.coursex.com/xsd/page}next" default="false" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
})
@XmlRootElement(name = "Page", namespace = "http://www.coursex.com/xsd/page")
public class Page implements Serializable {

    @XmlAttribute(name = "pageNum")
    protected Integer pageNum;
    @XmlAttribute(name = "active")
    protected Boolean active;
    @XmlAttribute(name = "previous")
    protected Boolean previous;
    @XmlAttribute(name = "next")
    protected Boolean next;


    /**
     * Gets the value of the pageNum property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public Integer getPageNum() {
        if (pageNum == null) {
            return new Integer("-1");
        } else {
            return pageNum;
        }
    }

    /**
     * Sets the value of the pageNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNum(Integer value) {
        this.pageNum = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isActive() {
        if (active == null) {
            return false;
        } else {
            return active;
        }
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActive(Boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the previous property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isPrevious() {
        if (previous == null) {
            return false;
        } else {
            return previous;
        }
    }

    /**
     * Sets the value of the previous property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrevious(Boolean value) {
        this.previous = value;
    }

    /**
     * Gets the value of the next property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isNext() {
        if (next == null) {
            return false;
        } else {
            return next;
        }
    }

    /**
     * Sets the value of the next property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNext(Boolean value) {
        this.next = value;
    }

}
