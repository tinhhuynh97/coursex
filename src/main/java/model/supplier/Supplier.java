
package model.supplier;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;


/**
 * <p>Java class for SupplierType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SupplierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SupplierName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SupplierUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LogoUrl" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", namespace = "http://www.coursex.com/xsd/supplier", propOrder = {
        "supplierName",
        "supplierUrl",
        "logoUrl"
})
@XmlRootElement(name = "Supplier", namespace = "http://www.coursex.com/xsd/supplier")
@Table(name = "supplier")
@Entity
public class Supplier implements Serializable {

    @XmlElement(name = "SupplierName", namespace = "http://www.coursex.com/xsd/supplier", required = true)
    protected String supplierName;
    @XmlElement(name = "SupplierUrl", namespace = "http://www.coursex.com/xsd/supplier", required = true)
    protected String supplierUrl;
    @XmlElement(name = "LogoUrl", namespace = "http://www.coursex.com/xsd/supplier")
    protected String logoUrl;
    @XmlAttribute(name = "id")
    protected Integer id;

    /**
     * Gets the value of the supplierName property.
     *
     * @return possible object is
     * {@link String }
     */
    @Column(name = "name")
    public String getSupplierName() {
        return supplierName;
    }

    /**
     * Sets the value of the supplierName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSupplierName(String value) {
        this.supplierName = value;
    }

    /**
     * Gets the value of the supplierUrl property.
     *
     * @return possible object is
     * {@link String }
     */
    @Column(name = "url")
    public String getSupplierUrl() {
        return supplierUrl;
    }

    /**
     * Sets the value of the supplierUrl property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSupplierUrl(String value) {
        this.supplierUrl = value;
    }

    /**
     * Gets the value of the logoUrl property.
     *
     * @return possible object is
     * {@link String }
     */
    @Column(name = "logo_url")
    public String getLogoUrl() {
        return logoUrl;
    }

    /**
     * Sets the value of the logoUrl property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLogoUrl(String value) {
        this.logoUrl = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * {@link Integer }
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setId(Integer value) {
        this.id = value;
    }

}
