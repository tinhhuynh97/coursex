package model;

import model.course.Course;

public class CourseAffinity implements Comparable<CourseAffinity> {
    private Course first;
    private Course second;
    private Double score;

    public CourseAffinity() {
    }

    public CourseAffinity(Course first, Course second) {
        this.first = first;
        this.second = second;
    }

    public CourseAffinity(Course first, Course second, Double score) {
        this.first = first;
        this.second = second;
        this.score = score;
    }

    public Course getFirst() {
        return first;
    }

    public void setFirst(Course first) {
        this.first = first;
    }

    public Course getSecond() {
        return second;
    }

    public void setSecond(Course second) {
        this.second = second;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public int compareTo(CourseAffinity o) {
        return this.score.compareTo(o.score);
    }
}
