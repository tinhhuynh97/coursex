
package model.course;

import model.category.Categories;
import model.supplier.Supplier;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "img",
        "name",
        "lecturerName",
        "sellPrice",
        "oldPrice",
        "discount",
        "url",
        "supplier",
        "categories"
})
@XmlRootElement(name = "Course", namespace = "http://www.coursex.com/xsd/course")
@XmlSeeAlso({Categories.class})
@Table(name = "course")
@Entity
public class Course implements Serializable {

    @XmlElement(namespace = "http://www.coursex.com/xsd/course", required = true)
    protected String img;
    @XmlElement(name = "Name", namespace = "http://www.coursex.com/xsd/course", required = true)
    protected String name;
    @XmlElement(name = "LecturerName", namespace = "http://www.coursex.com/xsd/course", required = true)
    protected String lecturerName = "";
    @XmlElement(name = "SellPrice", namespace = "http://www.coursex.com/xsd/course", required = true)
    protected Long sellPrice;
    @XmlElement(name = "OldPrice", namespace = "http://www.coursex.com/xsd/course")
    protected Long oldPrice;
    @XmlElement(name = "Discount", namespace = "http://www.coursex.com/xsd/course")
    @XmlSchemaType(name = "positiveInteger")
    protected Integer discount;
    @XmlElement(name = "Url", namespace = "http://www.coursex.com/xsd/course", required = true)
    protected String url;
    @XmlElement(name = "Supplier", namespace = "http://www.coursex.com/xsd/supplier")
    protected Supplier supplier;
    @XmlElement(name = "Categories", namespace = "http://www.coursex.com/xsd/categories")
    protected Categories categories;
    @XmlAttribute(name = "id")
    protected Integer id = 0;
    @XmlAttribute(name = "isDelete")
    protected Boolean isDelete;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Column(name = "course_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "lecturer_name")
    public String getLecturerName() {
        return lecturerName;
    }

    public void setLecturerName(String lecturerName) {
        this.lecturerName = lecturerName;
    }

    @Column(name = "sell_price")
    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    @Column(name = "old_price")
    public Long getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Long oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Transient
    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    @Transient
    public Categories getCategories() {
        if (categories == null) {
            categories = new Categories();
        }
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "supplier_id")
    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return "Course{" +
                "img='" + img + '\'' +
                ", name='" + name + '\'' +
                ", lecturerName='" + lecturerName + '\'' +
                ", sellPrice=" + sellPrice +
                ", oldPrice=" + oldPrice +
                ", discount=" + discount +
                ", url='" + url + '\'' +
                ", id=" + id +
                ", isDelete=" + isDelete +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Course) {
            Course other = (Course) obj;
            return this.img.equals(other.img) &&
                    this.name.equals(other.name) &&
                    this.lecturerName.equals(other.lecturerName) &&
                    this.sellPrice.equals(other.sellPrice) &&
                    this.url.equals(other.url);
        }
        return super.equals(obj);
    }

    public void from(Course course) {
        this.img = course.img;
        this.name = course.name;
        this.lecturerName = course.lecturerName;
        this.sellPrice = course.sellPrice;
        this.url = course.url;
    }
}
