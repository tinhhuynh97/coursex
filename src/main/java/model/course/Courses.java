
package model.course;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "course"
})
@XmlRootElement(name = "Courses", namespace = "http://www.coursex.com/xsd/courses")
public class Courses implements Serializable {

    @XmlElement(name = "Course", namespace = "http://www.coursex.com/xsd/course", required = true)
    protected List<Course> course;
    @XmlAttribute(name = "numOfCourses")
    protected Integer numOfCourses;

    public List<Course> getCourse() {
        return course;
    }

    public void setCourse(List<Course> course) {
        this.course = course;
    }

    public void setNumOfCourses(Integer numOfCourses) {
        this.numOfCourses = numOfCourses;
    }

    public void setInteger(Integer integer) {
        this.numOfCourses = integer;
    }
}
