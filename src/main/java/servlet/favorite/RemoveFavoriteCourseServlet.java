package servlet.favorite;

import config.ResponseCode;
import dao.user.UserDAO;
import model.response.GeneralResponse;
import model.user.User;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveFavoriteCourseServlet", urlPatterns = "/removeFavoriteCourses")
public class RemoveFavoriteCourseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");

        GeneralResponse xmlResponse = new GeneralResponse();
        xmlResponse.setCode(ResponseCode.FAILED.toString());

        if (email != null && !email.isEmpty()) {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByEmail(email);
            if (user != null) {
                int courseId = Integer.parseInt(request.getParameter("courseId"));
                int favoriteId = userDAO.checkExistingFavoriteCourse(user.getId(), courseId);
                if (favoriteId != -1) {
                    userDAO.updateFavoriteCourse(user.getId(), courseId, false);
                    xmlResponse.setCode(ResponseCode.SUCCESS.toString());
                }
            }
        }
        try {
            XMLUtils.marshallToOutputStream(response.getOutputStream(), xmlResponse, XMLUtils.getDefaultProperties());
        } catch (IOException e) {
            LogUtils.error(RemoveFavoriteCourseServlet.class.getName(), null, e);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
