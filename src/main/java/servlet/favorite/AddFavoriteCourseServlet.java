package servlet.favorite;

import config.ResponseCode;
import dao.user.UserDAO;
import model.response.GeneralResponse;
import model.user.User;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddFavoriteCourseServlet", urlPatterns = "/addFavoriteCourse")
public class AddFavoriteCourseServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String email = req.getParameter("email");

        GeneralResponse response = new GeneralResponse();
        response.setCode(ResponseCode.FAILED.toString());

        if (email != null && !email.isEmpty()) {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByEmail(email);
            if (user != null) {

                int courseId = Integer.parseInt(req.getParameter("courseId"));
                int favoriteId = userDAO.checkExistingFavoriteCourse(user.getId(), courseId);
                if (favoriteId != -1) {
                    if (userDAO.checkActiveFavoriteCourse(favoriteId)) {
                        response.setMessage("Bạn đã có khóa học này trong mục Yêu thích trước đó rồi!");
                    } else {
                        userDAO.updateFavoriteCourse(user.getId(), courseId, true);
                        response.setCode(ResponseCode.SUCCESS.toString());
                    }
                } else {
                    userDAO.addFavoriteCourse(user.getId(), courseId);
                    response.setCode(ResponseCode.SUCCESS.toString());
                }
            }
        }
        try {
            XMLUtils.marshallToOutputStream(resp.getOutputStream(), response, XMLUtils.getDefaultProperties());
        } catch (IOException e) {
            LogUtils.error(AddFavoriteCourseServlet.class.getName(), null, e);
        }
    }
}
