package servlet.auth;

import config.AuthCode;
import dao.user.UserDAO;
import model.response.AuthResponse;
import model.user.User;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserDAO userDAO = new UserDAO();
        User user = userDAO.authenticate(email, password);

        AuthResponse authResponse = new AuthResponse();
        if (user != null) {
            authResponse.setCode(AuthCode.LOGIN_SUCCESS.toString());
            authResponse.setUser(user);
        } else {
            authResponse.setCode(AuthCode.LOGIN_FAILED.toString());
        }
        response.setContentType("text/xml");
        XMLUtils.marshallToOutputStream(response.getOutputStream(), authResponse, XMLUtils.getDefaultProperties());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
