package servlet;

import config.Config;
import util.LogUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "FrontServlet", urlPatterns = "/home")
public class FrontServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processServlet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processServlet(request, response);
    }

    private void processServlet(HttpServletRequest request, HttpServletResponse response) {
        try {
            LogUtils.info(FrontServlet.class.getName(), "home");
            request.getRequestDispatcher(Config.HOME_PAGE_URL).forward(request, response);
        } catch (ServletException | IOException e) {
            LogUtils.error(FrontServlet.class.getName(), null, e);
        }
    }
}
