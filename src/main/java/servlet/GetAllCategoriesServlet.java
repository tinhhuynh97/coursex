package servlet;

import dao.category.CategoryDAO;
import model.category.Categories;
import model.category.Category;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "GetAllCategoriesServlet", urlPatterns = "/getAllCategories")
public class GetAllCategoriesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        List<Category> list = new CategoryDAO().findAll();
        Categories categories = new Categories();
        categories.setCategory(list);
        resp.setContentType("text/xml");
        try {
            XMLUtils.marshallToOutputStream(resp.getOutputStream(), categories, XMLUtils.getDefaultProperties());
        } catch (IOException e) {
            LogUtils.error(GetAllCategoriesServlet.class.getName(), null, e);
        }
    }
}
