package servlet.getCourses;

import config.Config;
import dao.course.CourseDAO;
import dao.user.UserDAO;
import model.CourseAffinity;
import model.course.Course;
import model.course.Courses;
import model.user.User;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "GetRecommendCoursesServlet", urlPatterns = "/getRecommendCourses")
public class GetRecommendCoursesServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processServlet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processServlet(request, response);
    }

    private void processServlet(HttpServletRequest request, HttpServletResponse response) {
        List<CourseAffinity> affinities = new ArrayList<>();
        String email = request.getParameter("email");
        if (email != null && !email.isEmpty()) {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getByEmail(email);
            if (user != null) {
                CourseDAO courseDAO = new CourseDAO();
                List<Course> favCourses = courseDAO.findAllFavorite();
                int totalFavoriteUserCount = userDAO.getUserFavoriteCount();
                for (Course first : favCourses) {
                    List<User> firstFavUsers = userDAO.findFavoriteByCourseId(first.getId());
                    for (Course second : favCourses) {
                        if (first.equals(second)) {
                            continue;
                        }
                        List<User> secFavUsers = userDAO.findFavoriteByCourseId(second.getId());
                        firstFavUsers.retainAll(secFavUsers);
                        List<User> commonFavUsers = intersect(firstFavUsers, secFavUsers);
                        double score = (double) commonFavUsers.size() / totalFavoriteUserCount;
                        CourseAffinity affinity = new CourseAffinity(first, second, score);
                        affinities.add(affinity);
                    }
                }
                Collections.sort(affinities);
                Collections.reverse(affinities);
                affinities.removeIf(courseAffinity -> courseAffinity.getScore() == 0.0);

                List<Course> myFavCourse = courseDAO.findFavoriteByUserId(user.getId());
                List<Course> recommendCourses = getRecommendCourses(myFavCourse, affinities);
                Courses courses = new Courses();
                courses.setCourse(recommendCourses);
                try {
                    XMLUtils.marshallToOutputStream(response.getOutputStream(), courses, XMLUtils.getDefaultProperties());
                } catch (IOException e) {
                    LogUtils.error(GetRecommendCoursesServlet.class.getName(), null, e);
                }
            }
        }
    }

    private List<Course> getRecommendCourses(List<Course> myFavCourse, List<CourseAffinity> affinities) {
        if (myFavCourse.isEmpty() || affinities.isEmpty()) {
            return null;
        }
        List<Course> recommendCoures = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (i < Config.DEFAULT_NUM_OF_RECOMMENDATION_COURSES) {
            if (j >= affinities.size()) {
                break;
            }
            CourseAffinity affinity = affinities.get(j);
            j++;
            Course first = affinity.getFirst();
            Course second = affinity.getSecond();
            if (myFavCourse.contains(first)) {
                if (!myFavCourse.contains(second) && !recommendCoures.contains(second)) {
                    recommendCoures.add(second);
                    i++;
                }
            } else if (myFavCourse.contains(second)) {
                if (!myFavCourse.contains(first) && !recommendCoures.contains(first)) {
                    recommendCoures.add(first);
                    i++;
                }
            }
        }

        return recommendCoures;
    }

    private List<User> intersect(List<User> A, List<User> B) {
        List<User> result = new LinkedList<>();
        for (User u : A) {
            if (B.contains(u)) {
                result.add(u);
            }
        }
        return result;
    }
}
