package servlet.getCourses;

import config.Config;
import config.Options;
import dao.course.CourseDAO;
import dao.user.UserDAO;
import model.course.Course;
import model.course.Courses;
import model.pagination.Pages;
import model.response.CourseListResponse;
import model.user.User;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet(name = "GetFavoriteCourses", urlPatterns = "/getFavoriteCourses")
public class GetFavoriteCourses extends BaseGetCoursesServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        int pageNum = 1;
        try {
            String email = request.getParameter("email");
            if (email != null && !email.isEmpty()) {
                UserDAO userDAO = new UserDAO();
                User user = userDAO.getByEmail(email);
                if (user != null) {
                    String pageNumParam = request.getParameter("page");
                    if (pageNumParam != null && !pageNumParam.isEmpty()) {
                        pageNum = Integer.parseInt(pageNumParam);
                        CourseDAO courseDAO = new CourseDAO();
                        int limit = Config.DEFAULT_COURSE_PAGING_LIMIT;
                        int offset = (pageNum - 1) * limit;
                        int pageSize = Config.DEFAULT_COURSE_PAGING_LIMIT;
                        Options options = setUpOptions(request);

                        List<Course> list =
                                courseDAO.findFavoriteByUserId(user.getId(), limit, offset, options);

                        Collections.reverse(list);

                        Courses courses = new Courses();
                        courses.setCourse(list);

                        int numOfCourses = courseDAO.getCountFavoriteByUserId(user.getId(), options);
                        Integer pageCount = numOfCourses / pageSize + ((numOfCourses % pageSize == 0) ? 0 : 1);
                        courses.setNumOfCourses(numOfCourses);

                        Pages pages = setUpPages(pageNum, pageCount);

                        CourseListResponse courseListResponse = new CourseListResponse();
                        courseListResponse.setCourses(courses);
                        courseListResponse.setPages(pages);
                        response.setContentType("text/xml");
                        XMLUtils.marshallToOutputStream(response.getOutputStream(), courseListResponse, XMLUtils.getDefaultProperties());
                    }
                }
            }
        } catch (NumberFormatException | IOException e) {
            LogUtils.error(GetFavoriteCourses.class.getName(), null, e);
        }
    }
}
