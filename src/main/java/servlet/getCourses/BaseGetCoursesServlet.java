package servlet.getCourses;

import config.Config;
import config.Options;
import config.SortCriteria;
import model.pagination.Pages;
import util.PaginationUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class BaseGetCoursesServlet extends HttpServlet {
    protected String sort = null;
    protected Integer minPrice = null;
    protected Integer maxPrice = null;


    protected Pages setUpPages(int selectedPageNum, int pageCount) {
        return PaginationUtils.setUpPages(selectedPageNum, pageCount, Config.DEFAULT_NUM_OF_PAGING_ITEM);
    }

    protected Options setUpOptions(HttpServletRequest req){
        sort = req.getParameter("sort");
        SortCriteria criteria = null;
        if (sort != null && !sort.isEmpty()) {
            criteria = SortCriteria.fromValue(sort);
        }

        try {
            minPrice = Integer.parseInt(req.getParameter("minPrice"));
        } catch (NumberFormatException e) {
            minPrice = null;
        }

        try {
            maxPrice = Integer.parseInt(req.getParameter("maxPrice"));
        } catch (NumberFormatException e) {
            maxPrice = null;
        }
        return new Options(criteria, minPrice, maxPrice);
    }
}
