package servlet.getCourses;

import config.Config;
import config.Options;
import dao.category.CategoryDAO;
import dao.course.CourseDAO;
import model.category.Category;
import model.course.Course;
import model.course.Courses;
import model.pagination.Pages;
import model.response.CourseListResponse;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "GetCoursesByCategoryServlet", urlPatterns = "/getCoursesByCategory")
public class GetCoursesByCategoryServlet extends BaseGetCoursesServlet {

    private String categoryName;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        int pageNum = 1;
        try {
            categoryName = req.getParameter("categoryName");
            String pageNumParam = req.getParameter("page");
            if (pageNumParam != null && !pageNumParam.isEmpty()) {
                pageNum = Integer.parseInt(pageNumParam);
            }
            CourseDAO courseDAO = new CourseDAO();
            int limit = Config.DEFAULT_COURSE_PAGING_LIMIT;
            int offset = (pageNum - 1) * limit;
            int pageSize = Config.DEFAULT_COURSE_PAGING_LIMIT;

            Options options = setUpOptions(req);

            Category category = new CategoryDAO().findByName(categoryName);
            List<Course> list =
                    courseDAO.findAllByCategory(category.getId(), limit, offset, options);

            Courses courses = new Courses();
            courses.setCourse(list);

            int numOfCourses = courseDAO.getCountAllByCategory(category.getId(), options);
            Integer pageCount = numOfCourses / pageSize + ((numOfCourses % pageSize == 0) ? 0 : 1);
            courses.setNumOfCourses(numOfCourses);

            Pages pages = setUpPages(pageNum, pageCount);

            CourseListResponse courseListResponse = new CourseListResponse();
            courseListResponse.setCourses(courses);
            courseListResponse.setPages(pages);
            resp.setContentType("text/xml");
            XMLUtils.marshallToOutputStream(resp.getOutputStream(), courseListResponse, XMLUtils.getDefaultProperties());
        } catch (NumberFormatException | IOException e) {
            LogUtils.error(GetCoursesByCategoryServlet.class.getName(), null, e);
        }
    }

}
