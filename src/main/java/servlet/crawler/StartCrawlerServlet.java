package servlet.crawler;

import config.AppCache;
import config.CrawlerStatus;
import crawler.MainCrawlingTask;
import dao.user.UserDAO;
import model.response.CrawlerResponse;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "StartCrawlerServlet", urlPatterns = "/startCrawler")
public class StartCrawlerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        UserDAO userDAO = new UserDAO();
        MainCrawlingTask task = MainCrawlingTask.getInstance(getServletContext());
        if (userDAO.checkAdminByEmail(email)) {
            if (task.getStatus() == CrawlerStatus.IDLING || task.getStatus() == CrawlerStatus.TERMINATED) {
                AppCache.setUpCache();
                task.startTask();
            }
        }
        CrawlerResponse crawlerResponse = new CrawlerResponse();
        crawlerResponse.setStatus(task.getStatus().toString());
        crawlerResponse.setDelay(task.getDelayForNextCrawl());
        response.setContentType("text/xml");
        try {
            XMLUtils.marshallToOutputStream(response.getOutputStream(), crawlerResponse, XMLUtils.getDefaultProperties());
        } catch (IOException e) {
            LogUtils.error(StartCrawlerServlet.class.getName(), "", e);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
