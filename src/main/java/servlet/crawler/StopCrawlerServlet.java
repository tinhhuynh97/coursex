package servlet.crawler;

import config.CrawlerStatus;
import crawler.MainCrawlingTask;
import dao.user.UserDAO;
import model.response.CrawlerResponse;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "StopCrawlerServlet", urlPatterns = "/stopCrawler")
public class StopCrawlerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        UserDAO userDAO = new UserDAO();
        MainCrawlingTask task = MainCrawlingTask.getInstance(getServletContext());
        if (userDAO.checkAdminByEmail(email)) {
            if (task.getStatus() == CrawlerStatus.WORKING || task.getStatus() == CrawlerStatus.PAUSED) {
                task.stopTask();
            }

        }
        CrawlerResponse crawlerResponse = new CrawlerResponse();
        crawlerResponse.setStatus(task.getStatus().toString());
        response.setContentType("text/xml");
        try {
            XMLUtils.marshallToOutputStream(response.getOutputStream(), crawlerResponse, XMLUtils.getDefaultProperties());
        } catch (IOException e) {
            LogUtils.error(StopCrawlerServlet.class.getName(), null, e);

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
