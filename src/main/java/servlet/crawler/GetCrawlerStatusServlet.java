package servlet.crawler;

import config.CrawlerStatus;
import crawler.MainCrawlingTask;
import dao.user.UserDAO;
import model.response.CrawlerResponse;
import util.LogUtils;
import util.XMLUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GetCrawlerStatusServlet", urlPatterns = "/getCrawlerStatus")
public class GetCrawlerStatusServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String email = request.getParameter("email");
        UserDAO userDAO = new UserDAO();
        if (userDAO.checkAdminByEmail(email)) {
            MainCrawlingTask task = MainCrawlingTask.getInstance(getServletContext());
            CrawlerResponse crawlerResponse = new CrawlerResponse();
            crawlerResponse.setStatus(task.getStatus().toString());
            if (task.getStatus() == CrawlerStatus.WORKING) {
                crawlerResponse.setDelay(task.getDelayForNextCrawl());
            }
            response.setContentType("text/xml");
            try {
                XMLUtils.marshallToOutputStream(response.getOutputStream(), crawlerResponse, XMLUtils.getDefaultProperties());
            } catch (IOException e) {
                LogUtils.error(GetCrawlerStatusServlet.class.getName(), "", e);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
