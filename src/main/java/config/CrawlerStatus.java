package config;

public enum CrawlerStatus {
    IDLING,
    STARTED,
    WORKING,
    PAUSED,
    SLEEPING,
    TERMINATED,
}
