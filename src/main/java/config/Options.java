package config;


public class Options {
    private SortCriteria sortCriteria;
    private Integer minPrice;
    private Integer maxPrice;

    public Options() {
    }

    public Options(SortCriteria sortCriteria, Integer minPrice, Integer maxPrice) {
        this.sortCriteria = sortCriteria;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }
}
