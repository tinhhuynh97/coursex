package config;


public class Config {
    public static String HOME_PAGE_URL = "index.jsp";
    public static String UNICA_URL = "https://unica.vn";
    public static String EDUMALL_URL = "http://edumall.vn";
    public static final String UNICA_NAME = "Unica";
    public static final String EDUMALL_NAME = "Edumall";
    public static String COURSE_XSD_PATH = "xsd/course.xsd";
    public static String SPLIT_WORD_REGEX = "/";
    public static int CRAWLING_INTERVAL_IN_DAY = 1;
    public static int DEFAULT_COURSE_PAGING_LIMIT = 20;
    public static int DEFAULT_NUM_OF_PAGING_ITEM = 7;
    public static int DEFAULT_NUM_OF_RECOMMENDATION_COURSES = 10;

    private Config() {

    }

}
