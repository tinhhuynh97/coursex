package config;


public enum SortCriteria {
    PRICE_ASC("priceAsc"),
    PRICE_DESC("priceDesc");

    private final String value;

    SortCriteria(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SortCriteria fromValue(String v) {
        for (SortCriteria c : SortCriteria.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        return null;
    }
}
