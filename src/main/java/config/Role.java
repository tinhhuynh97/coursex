package config;

public enum Role {
    NORMAL,
    ADMIN;

    public static Role fromValue(String v) {
        for (Role c : Role.values()) {
            if (c.toString().equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
