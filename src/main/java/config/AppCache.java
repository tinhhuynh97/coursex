package config;

import dao.SupplierDAO;
import dao.category.CategoryDAO;
import model.category.Category;
import model.category.dictionary.Dictionary;
import model.category.dictionary.Record;
import model.supplier.Supplier;

import java.util.ArrayList;
import java.util.List;

public class AppCache {
    public static List<Category> categories = new ArrayList<>();
    public static Dictionary dictionary;

    public static List<Supplier> suppliers;

    private AppCache() {

    }

    public static void setUpCache() {
        setUpCategories();
        setUpSuppliers();
        setUpDictionary();
    }

    private static void setUpSuppliers() {
        SupplierDAO supplierDAO = new SupplierDAO();
        suppliers = supplierDAO.findAll();
        suppliers.forEach(supplier -> {
            switch (supplier.getSupplierName()) {
                case Config.UNICA_NAME:
                    Config.UNICA_URL = supplier.getSupplierUrl();
                    break;
                case Config.EDUMALL_NAME:
                    Config.EDUMALL_URL = supplier.getSupplierUrl();
                    break;
            }
        });
    }

    private static void setUpCategories() {
        CategoryDAO categoryDAO = new CategoryDAO();
        AppCache.categories = categoryDAO.findAll();
    }

    private static void setUpDictionary() {
        if (dictionary == null) {
            dictionary = new Dictionary();
        } else {
            dictionary.clear();
        }
        CategoryDAO dao = new CategoryDAO();
        categories.forEach(category -> {
            String words = dao.getDictionaryWordsById(category.getId());
            if (words != null) {
                String[] wordList = words.split(Config.SPLIT_WORD_REGEX);
                Record record = new Record(category, wordList);
                dictionary.add(record);
            }
        });
    }
}
