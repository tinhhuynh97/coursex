package util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUtils {

    private LogUtils() {

    }

    public static void info(String className, String message) {
        Logger.getLogger(className).log(Level.INFO, message);
    }

    public static void error(String className, String message, Throwable exception) {
        Logger.getLogger(className).log(Level.SEVERE, message, exception);
    }
}
