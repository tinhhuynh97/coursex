package util;

import model.pagination.Page;
import model.pagination.Pages;

import java.util.ArrayList;
import java.util.List;

public class PaginationUtils {

    public static Pages setUpPages(int selectedPageNum, int pageCount, int numOfPagingItem) {
        if (numOfPagingItem % 2 == 0) {
            throw new IllegalArgumentException("Number of paging items must be an odd number");
        }
        Pages pages = new Pages();
        if (pageCount < 2) {
            return pages;
        }
        if (pageCount <= numOfPagingItem) {
            setUpNonSpecialFirstPages(selectedPageNum, pageCount, pages);
        } else {
            setUpSpecialPages(selectedPageNum, pageCount, numOfPagingItem, pages);
        }
        return pages;
    }

    private static void setUpNonSpecialFirstPages(int selectedPageNum, int pageCount, Pages pages) {
        for (int i = 1; i <= pageCount; i++) {
            Page page = createNormalPage(i, selectedPageNum);
            pages.getPage().add(page);
        }
    }

    private static void setUpSpecialPages(int selectedPageNum, int pageCount, int numOfPagingItem, Pages pages) {
        int middlePagePosition = numOfPagingItem / 2 + 1;
        if (selectedPageNum < middlePagePosition) {
            for (int i = 1; i <= numOfPagingItem; i++) {
                Page page = createNormalPage(i, selectedPageNum);
                pages.getPage().add(page);
            }
            addNextAndLastPages(selectedPageNum, pageCount, pages);
        } else if (selectedPageNum <= pageCount - middlePagePosition) {
            addPreviousAndFirstPages(selectedPageNum, pages);
            setUpMiddleNormalPage(selectedPageNum, numOfPagingItem, pages);
            addNextAndLastPages(selectedPageNum, pageCount, pages);
        } else if (selectedPageNum > pageCount - middlePagePosition) {
            addPreviousAndFirstPages(selectedPageNum, pages);
            for (int i = 1; i <= numOfPagingItem; i++) {
                Page page = createNormalPage(pageCount - numOfPagingItem + i, selectedPageNum);
                pages.getPage().add(page);
            }
        }
    }

    private static void setUpMiddleNormalPage(int selectedPageNum, int numOfPagingItems, Pages pages) {
        int offset = -numOfPagingItems / 2;
        int maxOffset = offset + numOfPagingItems;
        List<Integer> normalMiddlePageNum = new ArrayList<>();
        for (int j = offset; j < maxOffset; j++) {
            normalMiddlePageNum.add(selectedPageNum + j);
        }
        normalMiddlePageNum.forEach(pageNum -> {
            Page page = createNormalPage(pageNum, selectedPageNum);
            pages.getPage().add(page);
        });
    }

    private static void addPreviousAndFirstPages(int selectedPageNum, Pages pages) {
        Page first = new Page();
        first.setPageNum(1);

        Page previous = new Page();
        previous.setPageNum(selectedPageNum - 1);
        previous.setPrevious(true);

        pages.getPage().add(first);
        pages.getPage().add(previous);
    }

    private static void addNextAndLastPages(int selectedPageNum, int pageCount, Pages pages) {
        Page next = new Page();
        next.setPageNum(selectedPageNum + 1);
        next.setNext(true);

        Page last = new Page();
        last.setPageNum(pageCount);

        pages.getPage().add(next);
        pages.getPage().add(last);
    }

    private static Page createNormalPage(int position, int selectedPageNum) {
        Page page = new Page();
        page.setPageNum(position);
        page.setActive(position == selectedPageNum);
        return page;
    }
}
