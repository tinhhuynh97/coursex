package util;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class ValidatorErrorHandler implements ErrorHandler {
    private boolean valid = true;

    public boolean isValid() {
        return valid;
    }

    @Override
    public void warning(SAXParseException exception) {
        System.out.println("Warning: " + exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) {
        System.out.println("Error: " + exception.getMessage());
        valid = false;
    }

    @Override
    public void fatalError(SAXParseException exception) {
        System.out.println("Fatal error: " + exception.getMessage());
        valid = false;
    }
}
