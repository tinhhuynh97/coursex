package util;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class XMLUtils {

    private XMLUtils() {

    }

    public static XMLInputFactory getXMlInputFactory() {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
        factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);
        factory.setProperty(XMLInputFactory.IS_COALESCING, true);
        return factory;
    }

    public static XMLEventReader getEventReader(String content) throws XMLStreamException {
        InputStream stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
        return getXMlInputFactory()
                .createXMLEventReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
    }

    public static String getNextString(XMLEventReader reader) throws XMLStreamException {
        return reader.nextEvent().asCharacters().getData().trim();
    }

    public static String getAttributeValue(StartElement el, String attrName) {
        if (el == null || attrName == null) {
            return null;
        }
        Iterator iter = el.getAttributes();
        while (iter.hasNext()) {
            Attribute attr = (Attribute) iter.next();
            QName qName = attr.getName();
            if (qName.toString().equals(attrName)) {
                return attr.getValue();
            }
        }
        return null;
    }

    public static boolean hasAttributeValue(StartElement el, String attrName, String attrValue) {
        if (el == null || attrName == null || attrValue == null) {
            return false;
        }
        String value = getAttributeValue(el, attrName);
        return value != null && value.equals(attrValue);
    }

    public static <T> void marshalltoXML(String xmlFilePath, T obj, Map<String, Object> properties) {
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller ms = context.createMarshaller();
            setMarshallProperties(ms, properties);
            ms.marshal(obj, new File(xmlFilePath));
        } catch (JAXBException e) {
            LogUtils.error(XMLUtils.class.getName(), null, e);
        }
    }

    public static <T> void marshallToOutputStream(OutputStream outputStream, T obj, Map<String, Object> properties) {
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());
            Marshaller ms = context.createMarshaller();
            setMarshallProperties(ms, properties);
            ms.marshal(obj, outputStream);
        } catch (JAXBException e) {
            LogUtils.error(XMLUtils.class.getName(), null, e);
        }
    }

    private static void setMarshallProperties(Marshaller ms, Map<String, Object> properties) {
        if (properties != null) {
            properties.forEach((name, value) -> {
                try {
                    ms.setProperty(name, value);
                } catch (PropertyException e) {
                    LogUtils.error(XMLUtils.class.getName(), null, e);
                }
            });
        }
    }

    public static Map<String, Object> getDefaultProperties() {
        Map<String, Object> pros = new HashMap<>();
        pros.put(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        pros.put(Marshaller.JAXB_ENCODING, "UTF-8");
        return pros;
    }

    public static boolean validateXML(String xmlFilePath, String xsdFilePath) {
        boolean validate = false;
        try {
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema sc = sf.newSchema(new File(xsdFilePath));

            InputSource source = new InputSource(new BufferedReader(new FileReader(xmlFilePath)));

            Validator validator = sc.newValidator();
            ValidatorErrorHandler errorHandler = new ValidatorErrorHandler();

            validator.setErrorHandler(errorHandler);
            validator.validate(new SAXSource(source));

            validate = errorHandler.isValid();
        } catch (SAXException | IOException e) {
            LogUtils.error(XMLUtils.class.getName(), null, e);
        }
        return validate;
    }

    public static <T> boolean validateJAXBObject(T obj, String xsdFilePath) {
        boolean validate = false;
        ValidatorErrorHandler errorHandler = new ValidatorErrorHandler();
        try {
            JAXBContext jc = JAXBContext.newInstance(obj.getClass());
            JAXBSource source = new JAXBSource(jc, obj);

            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(xsdFilePath));

            Validator validator = schema.newValidator();
            validator.setErrorHandler(errorHandler);
            validator.validate(source);

            validate = errorHandler.isValid();
        } catch (SAXException | IOException | JAXBException e) {
            LogUtils.error(XMLUtils.class.getName(), null, e);
            validate = errorHandler.isValid();
        }
        return validate;
    }

}
